﻿using System;
using System.Collections.Generic;
using UnityEngine;


// day/hour, coins, hourly sales figures, etc.
[CreateAssetMenu(fileName = "BusinessRecords", menuName = "Data/BusinessRecords")]
public class BusinessRecords : ScriptableObject
{
	public int CurrentCoins;

	public int CurrentDay;
	public int CurrentHour;
	public int DayCount;			// incremented at midnight
	public int HourCount;			// total hours elapsed

	public int DayOpeningCoins;
	public int DayCupsToSell;
	public int DayCupsSold;
	public int DayCupSales;			// coins
	public int DayCupCost;			// coins
	public int DayAdPosters;

	public int PricePerCup;

	public int DrinkUnlockDays;         // number of profitable days required to unlock a new drink
	public int ProfitableDayCount;
	public int ConsecutiveProfitableDays;

	public DrinkInventorySO CurrentDrink;
	public int CostPerAd;

	//public const int MaxRecords = 90;

	public List<WeatherCondition> WeatherForecast = new List<WeatherCondition>();			// eg. 5 days

	public List<HourlyRecord> SalesRecords = new List<HourlyRecord>();

	public int PlayCount;           // no of times game has been restarted

	public int CurrentStallLevel = 1;

	// unlock a drink after x consecutive profitable days
	public bool CanUnlockDrink { get { return ConsecutiveProfitableDays > 0 && ConsecutiveProfitableDays % DrinkUnlockDays == 0; } }


	public void SetHourCount()
	{
		HourCount = ((DayCount - 1) * 24) + CurrentHour;
	}


	public void Init(int coins)
	{
		CurrentCoins = coins;

		CurrentDay = 1;
		CurrentHour = 0;
		DayCount = 1;            // incremented at midnight
		HourCount = 0;           // total hours elapsed

		DayOpeningCoins = 0;
		DayCupsToSell = 0;
		DayCupsSold = 0;
		DayCupSales = 0;        // coins
		DayCupCost = 0;         // coins
		DayAdPosters = 0;

		PricePerCup = 0;

		ProfitableDayCount = 0;
		ConsecutiveProfitableDays = 0;

		CurrentStallLevel = 1;

		CurrentDrink = null;

		SalesRecords.Clear();
		PlayCount = 0;          // no of times game has been restarted
	}


	public void ResetWeather(WeatherSO defaultWeather)
	{
		// first 5 days are fine weather
		for (int i = 0; i < WeatherForecast.Count; i++)
		{
			var randomTemp = UnityEngine.Random.Range(defaultWeather.MinDayTemp, defaultWeather.MaxDayTemp);

			WeatherForecast[i].SetWeather(defaultWeather, randomTemp);
		}
	}
}