﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayCycleManager : MonoBehaviour
{
	public AudioClip HourTick;
	public AudioClip DayTick;
	public AudioClip PauseAudio;

	public enum DayState
	{
		Morning,
		Day,
		Night,
		Invest
	}
	private DayState dayState = DayState.Morning;

	public float SecondsPerHour;					// seconds for each 'realtime' hour
	public float MorningNightSlowdown;				// slow clock during decision-making times!
	public float InvestSlowdown;                    // slow clock during decision-making times!

	public float playerClockSpeed = 1f;                 // can be changed by user in DayUI

	public static int DayStartHour = 6;       // switches to day UI - trading time - change price only
	public static int MidnightHour = 0;       // switches to morning UI - today's strategy / order stock
	public static int DayEndHour = 18;        // switches to night UI - day's results / order stock


	private float HourSeconds { get { return 60 * 60; } }
	private float ClockTickInterval { get { return HourSeconds / ((SecondsPerHour / playerClockSpeed) / SlowdownRate); } }

	private float SlowdownRate
	{
		get
		{
			if (dayState == DayState.Morning || dayState == DayState.Night)
				return MorningNightSlowdown;

			if (dayState == DayState.Invest)
				return InvestSlowdown;

			return 1f;
		}
	}

	public BusinessRecords BusinessRecords;         // SO for persistence!

	private bool clockRunning = false;
	private bool clockPaused = false;

	private Coroutine clockCoroutine;


	private void OnEnable()
	{
		ThirstyEvents.OnClockStart += StartClock;
		ThirstyEvents.OnClockTogglePause += OnClockTogglePause;
		ThirstyEvents.OnClockPaused += OnClockPaused;		
		ThirstyEvents.OnClockStop += StopClock;
		ThirstyEvents.OnClockSpeedChanged += OnClockSpeedChanged;

		ThirstyEvents.OnMorning += OnMorning;
		ThirstyEvents.OnDay += OnDay;
		ThirstyEvents.OnNight += OnNight;
		ThirstyEvents.OnInvest += OnInvest;
	}

	private void OnDisable()
	{
		ThirstyEvents.OnClockStart -= StartClock;
		ThirstyEvents.OnClockTogglePause -= OnClockTogglePause;
		ThirstyEvents.OnClockPaused -= OnClockPaused;
		ThirstyEvents.OnClockStop -= StopClock;
		ThirstyEvents.OnClockSpeedChanged -= OnClockSpeedChanged;

		ThirstyEvents.OnMorning -= OnMorning;
		ThirstyEvents.OnDay -= OnDay;
		ThirstyEvents.OnNight -= OnNight;
		ThirstyEvents.OnInvest -= OnInvest;
	}


	private void StartClock()
	{
		clockRunning = true;
		clockPaused = false;
		clockCoroutine = StartCoroutine(ClockTicker());

		ThirstyEvents.OnDayCount?.Invoke(BusinessRecords.DayCount, true);
	}


	private void OnClockTogglePause()
	{
		PauseClock(!clockPaused);
		ThirstyEvents.OnClockPaused?.Invoke(clockPaused);
	}

	private void OnClockPaused(bool paused)
	{
		PauseClock(paused);
	}

	private void PauseClock(bool paused)
	{
		if (PauseAudio != null && clockPaused != paused)
			AudioSource.PlayClipAtPoint(PauseAudio, Vector3.zero);

		clockPaused = paused;
	}

	private void StopClock()
	{
		if (clockCoroutine != null)
		{
			clockRunning = false;
			clockPaused = false;
			StopCoroutine(clockCoroutine);
			clockCoroutine = null;
		}
	}

	private void OnClockSpeedChanged(float speed)
	{
		playerClockSpeed = speed;
	}

	private IEnumerator ClockTicker()
	{
		while (clockRunning)
		{
			while (clockPaused)
			{
				yield return null;
			}

			yield return new WaitForSeconds(60 * 60 / ClockTickInterval);

			if (HourTick != null)
				AudioSource.PlayClipAtPoint(HourTick, Vector3.zero);

			BusinessRecords.CurrentHour++;

			if (BusinessRecords.CurrentHour >= 24)      // 11pm
			{
				BusinessRecords.CurrentHour = 0;
				BusinessRecords.DayCount++;
				ThirstyEvents.OnDayCount?.Invoke(BusinessRecords.DayCount, false);

				if (DayTick != null)
					AudioSource.PlayClipAtPoint(DayTick, Vector3.zero);
			}

			BusinessRecords.SetHourCount();

			ThirstyEvents.OnHour?.Invoke(BusinessRecords.DayCount, BusinessRecords.HourCount, BusinessRecords.CurrentHour, DayStartHour, DayEndHour, ClockTickInterval);

			if (BusinessRecords.CurrentHour == MidnightHour)
				ThirstyEvents.OnMorning?.Invoke();
			else if (BusinessRecords.CurrentHour == DayStartHour)
				ThirstyEvents.OnDay?.Invoke();
			else if (BusinessRecords.CurrentHour == DayEndHour)
				ThirstyEvents.OnNight?.Invoke();

			yield return null;
		}

		yield return null;
	}


	private void OnMorning()
	{
		dayState = DayState.Morning;
	}

	private void OnDay()
	{
		dayState = DayState.Day;

		if (BusinessRecords.CurrentHour != DayStartHour)    // eg. player jumped forward
		{
			if (BusinessRecords.CurrentHour > DayStartHour)		// skipping over midnight
			{
				BusinessRecords.DayCount++;
				ThirstyEvents.OnDayCount?.Invoke(BusinessRecords.DayCount, false);
			}

			BusinessRecords.CurrentHour = DayStartHour;
			BusinessRecords.SetHourCount();
			ThirstyEvents.OnHour?.Invoke(BusinessRecords.DayCount, BusinessRecords.HourCount, BusinessRecords.CurrentHour, DayStartHour, DayEndHour, ClockTickInterval);
		}
	}

	private void OnNight()
	{
		dayState = DayState.Night;

		if (BusinessRecords.CurrentHour != DayEndHour)		// eg. player jumped forward
		{
			BusinessRecords.CurrentHour = DayEndHour;
			BusinessRecords.SetHourCount();

			ThirstyEvents.OnHour?.Invoke(BusinessRecords.DayCount, BusinessRecords.HourCount, BusinessRecords.CurrentHour, DayStartHour, DayEndHour, ClockTickInterval);
		}
	}


	private void OnInvest()
	{
		dayState = DayState.Invest;
	}
}
