﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BusinessManager : MonoBehaviour
{
	public int StartupCoins = 100;
	public int AdCoins;
	public int MinCupsPerHour = 5;
	public int MaxCupsPerHour;
	public int MaxAdPosters;					// no effect if greater

	public BusinessRecords BusinessRecords;         // SO for persistence!

	private int currentHour;
	private int hourClosingCoins = 0;               // becomes opening for next hour
	private WeatherCondition currentWeather;        // can affect sales

	public enum GroupMindset
	{
		Normal,
		Healthy,
		Unhealthy,
		Scared,
		Excited
	}
	private GroupMindset currentMindset;


	private void OnEnable()
	{
		ThirstyEvents.OnHour += OnHour;
		ThirstyEvents.OnDayCount += OnDayCount;

		ThirstyEvents.OnDayCupsToSell += OnDayCupsToSell;
		ThirstyEvents.OnDayAdPosters += OnDayAdPosters;

		ThirstyEvents.OnDrinkSelected += OnDrinkSelected;
		ThirstyEvents.OnPlaceOrder += OnPlaceOrder;

		ThirstyEvents.OnWeatherChanged += OnWeatherChanged;
		ThirstyEvents.OnStallUpgrade += OnStallUpgrade;
		ThirstyEvents.OnAdWatched += OnAdWatched;

		ThirstyEvents.OnResetData += OnResetData;
	}

	private void OnDisable()
	{
		ThirstyEvents.OnHour -= OnHour;
		ThirstyEvents.OnDayCount -= OnDayCount;

		ThirstyEvents.OnDayCupsToSell -= OnDayCupsToSell;
		ThirstyEvents.OnDayAdPosters -= OnDayAdPosters;

		ThirstyEvents.OnDrinkSelected -= OnDrinkSelected;
		ThirstyEvents.OnPlaceOrder -= OnPlaceOrder;

		ThirstyEvents.OnWeatherChanged -= OnWeatherChanged;
		ThirstyEvents.OnStallUpgrade -= OnStallUpgrade;
		ThirstyEvents.OnAdWatched -= OnAdWatched;

		ThirstyEvents.OnResetData -= OnResetData;
	}


	private void Start()
	{
		if (BusinessRecords.PlayCount == 0 || BusinessRecords.CurrentCoins < StartupCoins)      // TODO: review
		{
			InitStartupCoins();
			//BusinessRecords.ResetHourDay();
		}

		BusinessRecords.PlayCount++;

		//BusinessRecords.SalesRecords.Clear();      // TODO: remove this and cap record count. remove hourly records on day count?
		BusinessRecords.DayOpeningCoins = BusinessRecords.CurrentCoins;

		hourClosingCoins = BusinessRecords.CurrentCoins;
	}


	private void UpdateHourlySales()
	{
		if (BusinessRecords.DayCupsSold >= BusinessRecords.DayCupsToSell)                   // already sold out
			return;

		var cupsSold = CalculateDrinkSales();

		if (cupsSold + BusinessRecords.DayCupsSold >= BusinessRecords.DayCupsToSell)         // limit to stock in stall
		{
			cupsSold = BusinessRecords.DayCupsToSell - BusinessRecords.DayCupsSold;
			ThirstyEvents.OnSoldOut?.Invoke();
		}

		var salesCoins = cupsSold * BusinessRecords.PricePerCup;
		var costCoins = cupsSold * BusinessRecords.CurrentDrink.Drink.CostPerCup;

		// cost calculated for all unsold drinks (now spoilt)
		if (BusinessRecords.CurrentHour == DayCycleManager.DayEndHour)
		{
			int unsoldDrinks = BusinessRecords.DayCupsToSell - BusinessRecords.DayCupsSold;
			costCoins += unsoldDrinks * BusinessRecords.CurrentDrink.Drink.CostPerCup;
		}

		BusinessRecords.DayCupsSold += cupsSold;
		BusinessRecords.DayCupSales += salesCoins;
		BusinessRecords.DayCupCost += costCoins;

		var record = new HourlyRecord
		{
			Period = HourlyRecord.RecordPeriod.Hourly,
			Drink = BusinessRecords.CurrentDrink,

			CupsSold = cupsSold,
			DrinkSales = salesCoins,
			DrinkCost = costCoins,

			OpeningCoins = hourClosingCoins,			// previous hour closing
			ClosingCoins = BusinessRecords.CurrentCoins,

			DayNumber = BusinessRecords.CurrentDay,
			Hour = currentHour
		};

		BusinessRecords.SalesRecords.Add(record);
		ThirstyEvents.OnHourlySalesRecord?.Invoke(record);

		UpdateCoins(salesCoins);

		hourClosingCoins = BusinessRecords.CurrentCoins;           // = opening for next hour
	}

	// calculate hourly drink sales based on
	// price, weather and ad poster factors: 0 = no sales, 1 = max sales
	// factors applied to a random number so outcome not predictable
	// final result mutiplied by stall level
	private int CalculateDrinkSales()
	{
		DrinkSO drink = BusinessRecords.CurrentDrink.Drink;

		// clamp price to max for this calculation
		float pricePerCup = (BusinessRecords.PricePerCup > drink.MaxPricePerCup) ?
												drink.MaxPricePerCup : BusinessRecords.PricePerCup;

		float priceFactor = 1f - (pricePerCup / drink.MaxPricePerCup);						// 0 = no sales
		float weatherFactor = drink.WeatherFactor(currentWeather.Weather.WeatherType);      // 0 = no sales
		float adPosterFactor = (float)BusinessRecords.DayAdPosters / (float)MaxAdPosters;   // 0 = no sales

		//int minCupsPerHour = (BusinessRecords.PlayCount == 0) ? MaxCupsPerHour : MinCupsPerHour;
		int drinkRandom = UnityEngine.Random.Range(MinCupsPerHour, MaxCupsPerHour);
		Debug.Log("CalculateDrinkSales: priceFactor = " + priceFactor + " weatherFactor = " + weatherFactor + " adPosterFactor = " + adPosterFactor + " drinkRandom = " + drinkRandom);

		return (int) Mathf.Ceil(drinkRandom * priceFactor * weatherFactor * adPosterFactor) * BusinessRecords.CurrentStallLevel;
	}

	private void InitStartupCoins()
	{
		BusinessRecords.CurrentCoins = StartupCoins;
		ThirstyEvents.OnCoinsChanged?.Invoke(BusinessRecords.CurrentCoins);
	}

	private void UpdateCoins(int coins)
	{
		BusinessRecords.CurrentCoins += coins;

		if (BusinessRecords.CurrentCoins < 0)
		{
			BusinessRecords.CurrentCoins = 0;
			ThirstyEvents.OnBankrupted?.Invoke(BusinessRecords.CurrentCoins);
		}

		ThirstyEvents.OnCoinsChanged?.Invoke(BusinessRecords.CurrentCoins);
	}


	private void OnPlaceOrder(int totalOrderValue)
	{
		UpdateCoins(-totalOrderValue);
	}

	private void OnDrinkSelected(DrinkInventorySO drink)
	{
		BusinessRecords.CurrentDrink = drink;
	}

	private void OnDayCupsToSell(int changeQty, int cupsToSell)
	{
		if (BusinessRecords.DayCupsToSell == 0 && changeQty == 0)
			return;

		BusinessRecords.DayCupsToSell = cupsToSell;

		if (BusinessRecords.DayCupsToSell < 0)
			BusinessRecords.DayCupsToSell = 0;

		UpdateCoins(-BusinessRecords.CurrentDrink.Drink.CostPerCup * changeQty);

		ThirstyEvents.OnDrinkStockIssued?.Invoke(BusinessRecords.CurrentDrink.Drink.DrinkName, changeQty, cupsToSell);
	}


	private void OnDayAdPosters(int changeQty, int adPosters)
	{
		if (changeQty == 0)
			return;

		BusinessRecords.DayAdPosters = adPosters;

		if (BusinessRecords.DayAdPosters < 0)
			BusinessRecords.DayAdPosters = 0;

		UpdateCoins(-BusinessRecords.CostPerAd * changeQty);
	}

	private void OnHour(int dayCount, int hourCount, int hour, int dayStartHour, int dayEndHour, float clockTickInterval)
	{
		currentHour = hour;

		if (currentHour > dayStartHour && currentHour <= dayEndHour)
			UpdateHourlySales();
	}

	private void OnDayCount(int day, bool startUp)
	{
		// log a daily summary record
		var record = new HourlyRecord
		{
			Period = HourlyRecord.RecordPeriod.Daily,
			Drink = BusinessRecords.CurrentDrink,

			CupsSold = BusinessRecords.DayCupsSold,
			DrinkSales = BusinessRecords.DayCupSales,
			DrinkCost = BusinessRecords.DayCupCost,

			OpeningCoins = BusinessRecords.DayOpeningCoins,   
			ClosingCoins = BusinessRecords.CurrentCoins,

			DayNumber = BusinessRecords.CurrentDay,
			Hour = currentHour,

			Weather = currentWeather
		};

		BusinessRecords.SalesRecords.Add(record);
		ThirstyEvents.OnDailySalesRecord?.Invoke(record);

		BusinessRecords.DayOpeningCoins = BusinessRecords.CurrentCoins;

		BusinessRecords.DayCupsSold = 0;
		BusinessRecords.DayCupSales = 0;
		BusinessRecords.DayCupCost = 0;

		BusinessRecords.CurrentDay = day;
	}

	private void OnWeatherChanged(int dayNum, int forecastIndex, WeatherCondition weatherCondition)
	{
		if (forecastIndex == 0)			// today's weather
		{
			currentWeather = weatherCondition;
		}
	}

	private void OnStallUpgrade(int stallLevel, int coins)
	{
		BusinessRecords.CurrentStallLevel = stallLevel;

		UpdateCoins(-coins);
	}


	private void OnAdWatched()
	{
		UpdateCoins(AdCoins);
	}


	private void OnResetData()
	{
		InitBusinessRecords();

		ThirstyEvents.OnCoinsChanged?.Invoke(BusinessRecords.CurrentCoins);
		ThirstyEvents.OnHour?.Invoke(BusinessRecords.DayCount, BusinessRecords.CurrentHour, BusinessRecords.HourCount, DayCycleManager.DayStartHour, DayCycleManager.DayEndHour, 0f);
		ThirstyEvents.OnDayCount?.Invoke(BusinessRecords.DayCount, false);
	}

	private void InitBusinessRecords()
	{
		BusinessRecords.Init(StartupCoins);
	}

}

