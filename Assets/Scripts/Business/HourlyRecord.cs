﻿using System;
using UnityEngine;


// hourly and daily sales figures
[Serializable]
public class HourlyRecord
{
	public enum RecordPeriod
	{
		Hourly,
		Daily
	}

	public RecordPeriod Period;
	public int DayNumber;
	public int Hour;

	public DrinkInventorySO Drink;

	public int OpeningCoins;
	public int CupsSold;
	public int DrinkSales;
	public int DrinkCost;      
	public int ClosingCoins;

	public WeatherCondition Weather;


	public string ToJson()
	{
		return JsonUtility.ToJson(this);
	}
}