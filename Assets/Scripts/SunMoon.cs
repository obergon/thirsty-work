﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunMoon : MonoBehaviour
{
	public Light SunLight;           // main directional light
	public Light MoonLight;

	public float LightRotateTime;

	public Color SunMiddayColour;
	public Color SunDuskDawnColour;

	public Color MoonMidnightColour;
	public Color MoonDuskDawnColour;

	public float LightMoveSpeed;            // fraction of the hourly interval sun/moon take to move on each hour

	public BusinessRecords BusinessRecords;

	private int lastHourCount;

	private readonly int degreesPerHour = 360 / 24;		// ie. 15 deg per hour


	private void Start()
	{
		SunLight.color = SunDuskDawnColour;
		MoonLight.color = MoonDuskDawnColour;

		RotateSunMoon(BusinessRecords.CurrentHour, 0.5f);	// init to current time of day

		RotateSunColours();
		RotateMoonColours();
	}

	private void OnEnable()
	{
		ThirstyEvents.OnHour += OnHour;
	}

	private void OnDisable()
	{
		ThirstyEvents.OnHour -= OnHour;
	}


	private void OnHour(int dayCount, int hourCount, int hour, int dayStartHour, int dayEndHour, float clockTickInterval)
	{
		RotateSunMoon(hourCount, clockTickInterval / LightMoveSpeed);
	}

	private void RotateSunMoon(int hourCount, float rotateTime)
	{
		int hoursElapsed = hourCount - lastHourCount;

		LeanTween.rotateAround(SunLight.gameObject, SunLight.transform.right, degreesPerHour * hoursElapsed, rotateTime)
											.setEase(LeanTweenType.easeOutCubic);

		LeanTween.rotateAround(MoonLight.gameObject, MoonLight.transform.right, degreesPerHour * hoursElapsed, rotateTime)
											.setEase(LeanTweenType.easeOutCubic);

		lastHourCount = hourCount;
	}


	//TODO: not in sync with time of day, but looks cool anyway
	// yellow - red
	private void RotateSunColours()
	{
		LeanTween.value(gameObject, SunDuskDawnColour, SunMiddayColour, LightRotateTime)
				.setEase(LeanTweenType.linear)
				.setOnUpdate((Color val) =>
				{
					SunLight.color = val;
				})
				.setLoopPingPong();
	}

	//TODO: not in sync with time of day, but looks cool anyway
	// blue - purple
	private void RotateMoonColours()
	{
		LeanTween.value(gameObject, MoonDuskDawnColour, MoonMidnightColour, LightRotateTime)
				.setEase(LeanTweenType.linear)
				.setOnUpdate((Color val) =>
				{
					MoonLight.color = val;
				})
				.setLoopPingPong();
	}
}
