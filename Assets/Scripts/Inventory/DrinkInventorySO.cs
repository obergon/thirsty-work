﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "DrinkInventory", menuName = "Data/DrinkData/DrinkInventory")]
public class DrinkInventorySO : ScriptableObject
{
    public DrinkSO Drink;

    public int OnOrder;
    public int ToOrder;

    public List<DrinkReceiptSO> Receipts = new List<DrinkReceiptSO>();
    public int InStock { get { return Receipts.Sum(x => x.QtyInStock); }}

    public int Spoilt;

    public void ReceiveStock(int dayNum, int qty)
    {
        if (qty <= 0)
            return;

        var newReceipt = new DrinkReceiptSO();

        newReceipt.DayNumber = dayNum;
        newReceipt.QtyReceived = newReceipt.QtyInStock = qty;

        Receipts.Add(newReceipt);

        ThirstyEvents.OnDrinkStockChanged?.Invoke(Drink.DrinkName, qty, InStock);
    }

	// returns new stock level
    public int IssueStock(int issueQty)
    {
        if (issueQty > InStock)						// won't issue if more than stock qty
            return InStock;

        int qtyToIssue = issueQty;

        foreach (var receipt in Receipts)
        {
            if (receipt.QtyInStock <= 0)
                continue;

            if (receipt.QtyInStock >= qtyToIssue)		// first receipt has enough stock
            {
                receipt.QtyInStock -= qtyToIssue;
                qtyToIssue = 0;
            }
            else									// receipt stock less than issue qty
            {
                qtyToIssue -= receipt.QtyInStock;
                //Receipts.RemoveAt(0);
            }

            if (qtyToIssue <= 0)
                break;
        }

        ThirstyEvents.OnDrinkStockChanged?.Invoke(Drink.DrinkName, issueQty, InStock);
        return InStock;
    }

    public void CheckSpoilage(int currentDayNum)
    {
        foreach (var receipt in Receipts)
        {
            if (receipt.QtyInStock <= 0)
                continue;

            if (currentDayNum - receipt.DayNumber > Drink.ShelfLife)
            {
                UpdateSpoiltQty(receipt.QtyInStock);
                IssueStock(receipt.QtyInStock);			// stock lost
            }
        }
    }

    public int UpdateSpoiltQty(int qtyIncrease)
    {
        if (qtyIncrease == 0)
            return Spoilt;

        Spoilt += qtyIncrease;

        if (Spoilt < 0)
            Spoilt = 0;

		ThirstyEvents.OnDrinkSpoiltChanged?.Invoke(Drink.DrinkName, qtyIncrease, Spoilt);
		return Spoilt;
    }

    public int SetSpoiltQty(int spoiltQty)
    {
        var change = Spoilt - spoiltQty;

        Spoilt = spoiltQty;

        if (Spoilt < 0)
            Spoilt = 0;

        ThirstyEvents.OnDrinkSpoiltChanged?.Invoke(Drink.DrinkName, change, Spoilt);
        return Spoilt;
    }

    public int UpdateOnOrderQty(int qty)
    {
        if (qty == 0)
            return OnOrder;

        OnOrder += qty;

        if (OnOrder < 0)
            OnOrder = 0;

		ThirstyEvents.OnDrinkOnOrderChanged?.Invoke(Drink.DrinkName, qty, OnOrder, Drink.CostPerCup);
		return OnOrder;
    }

    public int UpdateToOrderQty(int qty)
    {
        if (qty == 0)
            return ToOrder;

        ToOrder += qty;

        if (ToOrder < 0)
            ToOrder = 0;

		ThirstyEvents.OnDrinkToOrderChanged?.Invoke(Drink.DrinkName, qty, ToOrder, Drink.CostPerCup);
		return ToOrder;
    }

	// to order -> on order
    public void PlaceOrder()
    {
        UpdateOnOrderQty(ToOrder);
        UpdateToOrderQty(-ToOrder);
    }

	// on order -> in stock
    public void ReceiveOnOrder(int dayNum)
    {
        ReceiveStock(dayNum, OnOrder);
        UpdateOnOrderQty(-OnOrder);
    }
}
