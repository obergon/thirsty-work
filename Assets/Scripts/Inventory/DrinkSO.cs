﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Drink", menuName = "Data/DrinkData/Drink")]
public class DrinkSO : ScriptableObject
{
    public Sprite DrinkSprite;
    public string DrinkName;
    public Color DrinkColour;

	public bool Unlocked = false;

    public int CostPerCup;
    public int MaxPricePerCup;			// zero sales if greater

    public int ShelfLife;
    public int PackQty;

    public int ServesPerHour;

    public List<WeatherRating> WeatherRatings;

    [Range(0,1)]
    public float HealthyRating;
    [Range(0, 1)]
    public float PopularityRating;
    [Range(0, 1)]
    public float AddictiveRating;


	// get this drink's rating for the given weather
    public float WeatherFactor(WeatherController.WeatherType weather)
    {
        foreach (var weatherRating in WeatherRatings)
        {
            if (weatherRating.Weather == weather)
                return weatherRating.Rating;
        }
        return 0;
    }
}

[Serializable]
public class WeatherRating
{
    public WeatherController.WeatherType Weather;
    [Range(0, 1)]
    public float Rating;        // 0 == unsuitable for weather (zero sales)
}
