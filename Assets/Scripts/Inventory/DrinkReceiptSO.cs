﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


// list of these in DrinkInventorySO
[Serializable]
public class DrinkReceiptSO
{
    public int DayNumber;
    public int QtyReceived;
    public int QtyInStock;
}
