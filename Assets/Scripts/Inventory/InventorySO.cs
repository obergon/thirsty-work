﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Inventory", menuName = "Data/DrinkData/Inventory")]
public class InventorySO : ScriptableObject
{
    public List<DrinkInventorySO> Inventory = new List<DrinkInventorySO>();

	// could be converted to a dictionary, but there are only a few drinks available...
	public DrinkInventorySO LookupDrink(string drinkName)
	{
		foreach (DrinkInventorySO drinkInv in Inventory)
		{
			if (string.Equals(drinkInv.Drink.DrinkName, drinkName))
				return drinkInv;
		}

		return null;
	}
}
