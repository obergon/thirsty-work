﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InventoryManager : MonoBehaviour
{
	public InventorySO Inventory;
	public BusinessRecords BusinessRecords;		// SO

	private void OnEnable()
	{
		ThirstyEvents.OnDayCount += OnDayCount;
		ThirstyEvents.OnPlaceOrder += OrderStock;
		ThirstyEvents.OnDay += OnDay;
		ThirstyEvents.OnNight += OnNight;

		ThirstyEvents.OnResetData += OnResetData;
	}

	private void OnDisable()
	{
		ThirstyEvents.OnDayCount -= OnDayCount;
		ThirstyEvents.OnPlaceOrder -= OrderStock;
		ThirstyEvents.OnDay -= OnDay;
		ThirstyEvents.OnNight -= OnNight;

		ThirstyEvents.OnResetData -= OnResetData;
	}

	private void OrderStock(int totalOrderValue)
	{
		foreach (var drinkInventory in Inventory.Inventory)
		{
			drinkInventory.PlaceOrder();		// from to order to on order
		}
	}

	private void ReceiveOnOrder()
	{
		foreach (var drinkInventory in Inventory.Inventory)
		{
			drinkInventory.ReceiveOnOrder(BusinessRecords.CurrentDay);
		}
	}

	// midnight reconciliation
	private void OnDayCount(int day, bool startUp)
	{
		ReceiveOnOrder();
	}

	private void OnNight()
	{
		CheckForSpoilage();
	}

	private void OnDay()
	{
		ResetSpoilage();
	}

	// zero spoilage at start of each day
	private void ResetSpoilage()
	{
		foreach (var drinkInventory in Inventory.Inventory)
		{
			drinkInventory.SetSpoiltQty(0);
		}
	}

	// update spoilage at end of each day
	private void CheckForSpoilage()
	{
		foreach (var drinkInventory in Inventory.Inventory)
		{
			drinkInventory.CheckSpoilage(BusinessRecords.CurrentDay);
		}
	}

	private void OnResetData()
	{
		InitInventory();
	}

	private void InitInventory()
	{
		foreach (var drink in Inventory.Inventory)
		{
			drink.OnOrder = 0;
			drink.ToOrder = 0;

			drink.Drink.Unlocked = drink == Inventory.Inventory[0];		// unlock first drink only

			drink.Receipts.Clear();

			drink.Receipts.Add(new DrinkReceiptSO
			{
				DayNumber = 1,
				QtyReceived = drink.Drink.PackQty,
				QtyInStock = drink.Drink.PackQty
			});

			drink.Spoilt = 0;
		}
	}
}
