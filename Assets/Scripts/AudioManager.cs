﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource BirdsCrickets;
    public AudioSource CityTraffic;

    public AudioSource WelcomeMusic;
    public AudioSource NightMusic;		// until midnight
    public AudioSource MorningMusic;    // after midnight

	public AudioClip DrinkUnlockedAudio;
	public AudioClip DrinkSelectedAudio;


	private void OnEnable()
	{
		ThirstyEvents.OnWelcome += OnWelcome;
		ThirstyEvents.OnDay += OnDay;
		ThirstyEvents.OnMorning += OnMorning;
		ThirstyEvents.OnNight += OnNight;

		ThirstyEvents.OnDrinkSelected += OnDrinkSelected;
		ThirstyEvents.OnDrinkUnlocked += OnDrinkUnlocked;
	}

	private void OnDisable()
	{
		ThirstyEvents.OnWelcome -= OnWelcome;
		ThirstyEvents.OnDay -= OnDay;
		ThirstyEvents.OnMorning -= OnMorning;
		ThirstyEvents.OnNight -= OnNight;

		ThirstyEvents.OnDrinkSelected -= OnDrinkSelected;
		ThirstyEvents.OnDrinkUnlocked -= OnDrinkUnlocked;
	}


	private void OnWelcome()
	{
		WelcomeMusic.Play();
		NightMusic.Stop();
		MorningMusic.Stop();

		CityTraffic.Stop();
		BirdsCrickets.Stop();
	}

	// 6pm
	private void OnNight()
	{
		WelcomeMusic.Stop();
		NightMusic.Play();
		MorningMusic.Stop();

		CityTraffic.Stop();
		BirdsCrickets.Play();
	}

	// midnight
	private void OnMorning()
	{
		WelcomeMusic.Stop();
		NightMusic.Stop();
		MorningMusic.Play();

		CityTraffic.Stop();
		BirdsCrickets.Play();
	}

	// 6am
	private void OnDay()
	{
		WelcomeMusic.Stop();
		NightMusic.Stop();
		MorningMusic.Stop();

		CityTraffic.Play();
		BirdsCrickets.Play();
	}


	private void OnDrinkUnlocked(DrinkInventorySO drink)
	{
		if (DrinkUnlockedAudio != null)
			AudioSource.PlayClipAtPoint(DrinkUnlockedAudio, Vector3.zero);
	}

	private void OnDrinkSelected(DrinkInventorySO drink)
	{
		if (DrinkSelectedAudio != null)
			AudioSource.PlayClipAtPoint(DrinkSelectedAudio, Vector3.zero);
	}
}
