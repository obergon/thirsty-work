﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WelcomeUI : MonoBehaviour
{
	public Image StartOfDayPanel;			// hidden after first play
	public Button ContinueButton;			// ...start night
	public Button ResetButton;              // init business & inventory SO data

	public BusinessRecords BusinessRecords;

	private void Start()
	{
		ThirstyEvents.OnWelcome?.Invoke();

		StartOfDayPanel.gameObject.SetActive(BusinessRecords.PlayCount <= 1);
	}

	private void OnEnable()
	{
		ContinueButton.onClick.AddListener(OnContinueClick);
		ResetButton.onClick.AddListener(OnResetClick);
	}

	private void OnDisable()
	{
		ContinueButton.onClick.RemoveListener(OnContinueClick);
		ResetButton.onClick.RemoveListener(OnResetClick);
	}


	private void OnContinueClick()
	{
		ThirstyEvents.OnClockStart?.Invoke();

		if (BusinessRecords.CurrentHour < DayCycleManager.DayStartHour)
			ThirstyEvents.OnMorning?.Invoke();
		else if (BusinessRecords.CurrentHour >= DayCycleManager.DayEndHour)
			ThirstyEvents.OnNight?.Invoke();
		else
			ThirstyEvents.OnDay?.Invoke();
	}


	private void OnResetClick()
	{
		ThirstyEvents.OnResetData?.Invoke();
	}
}
