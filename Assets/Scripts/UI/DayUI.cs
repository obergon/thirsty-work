﻿
using System;
using UnityEngine;
using UnityEngine.UI;


public class DayUI : MonoBehaviour
{
	public Button EndDayButton;        // ...start night

	public Text SelectedDrink;
	public Text CupsSold;
	public Text CupSales;           // coins

	public Button PriceUpButton;
	public Button PriceDownButton;
	public Text PricePerCup;

	public Slider SpeedSlider;

	public BusinessRecords BusinessRecords;         // SO


	private void OnEnable()
	{
		ThirstyEvents.OnDrinkSelected += OnDrinkSelected;
		ThirstyEvents.OnHourlySalesRecord += OnHourlySalesRecord;
		ThirstyEvents.OnPricePerCupChanged += OnPricePerCupChanged;

		PriceUpButton.onClick.AddListener(OnPriceUpClick);
		PriceDownButton.onClick.AddListener(OnPriceDownClick);

		EndDayButton.onClick.AddListener(OnEndDayClick);
		SpeedSlider.onValueChanged.AddListener(OnSpeedChanged);

		SetSelectedDrink();
		SetPricePerCup();
		SetSales();
	}

	private void OnDisable()
	{
		ThirstyEvents.OnDrinkSelected -= OnDrinkSelected;
		ThirstyEvents.OnHourlySalesRecord -= OnHourlySalesRecord;
		ThirstyEvents.OnPricePerCupChanged -= OnPricePerCupChanged;

		PriceUpButton.onClick.RemoveListener(OnPriceUpClick);
		PriceDownButton.onClick.RemoveListener(OnPriceDownClick);

		EndDayButton.onClick.RemoveListener(OnEndDayClick);
		SpeedSlider.onValueChanged.RemoveListener(OnSpeedChanged);
	}

	private void SetPricePerCup()
	{
		PricePerCup.text = (BusinessRecords.PricePerCup > 0) ? BusinessRecords.PricePerCup.ToString() : "Free";
	}

	private void OnPricePerCupChanged(int pricePerCup)
	{
		BusinessRecords.PricePerCup = pricePerCup;
		SetPricePerCup();
	}

	private void OnPriceUpClick()
	{
		BusinessRecords.PricePerCup++;

		PricePerCup.text = BusinessRecords.PricePerCup.ToString();
		ThirstyEvents.OnPricePerCupChanged?.Invoke(BusinessRecords.PricePerCup);
	}

	private void OnPriceDownClick()
	{
		BusinessRecords.PricePerCup--;

		if (BusinessRecords.PricePerCup <= 0)
			BusinessRecords.PricePerCup = 1;

		PricePerCup.text = BusinessRecords.PricePerCup.ToString();
		ThirstyEvents.OnPricePerCupChanged?.Invoke(BusinessRecords.PricePerCup);
	}

	private void OnSpeedChanged(float speed)
	{
		ThirstyEvents.OnClockSpeedChanged?.Invoke(speed);
	}


	private void SetSelectedDrink()
	{
		if (BusinessRecords.CurrentDrink == null)
			Debug.LogError("SetSelectedDrink: null drink!");
		else
			SelectedDrink.text = BusinessRecords.CurrentDrink.Drink.DrinkName;
	}

	private void SetSales()
	{
		CupsSold.text = BusinessRecords.DayCupsSold.ToString() + "/" + BusinessRecords.DayCupsToSell.ToString();
		CupSales.text = BusinessRecords.DayCupSales.ToString();
	}

	private void OnHourlySalesRecord(HourlyRecord record)
	{
		SetSales();
	}

	private void OnEndDayClick()
	{
		ThirstyEvents.OnNight?.Invoke();
	}

	private void OnDrinkSelected(DrinkInventorySO drink)
	{
		SelectedDrink.text = drink.Drink.DrinkName;
	}
}
