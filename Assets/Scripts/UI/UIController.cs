﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public Image WelcomePanel;
    public Image MorningPanel;
    public Image DayPanel;
    public Image NightPanel;
    public Image InvestPanel;
    public Image ExpandPanel;
	public Image InventoryPanel;

	public Image blackOut;
	public Color blackOutColour;
	public float fadeToBlackTime;

	public AudioClip FadeInAudio;

	public List<Color> ColourPalette;		// TODO: not currently used

	private bool blackedOut;

	public Image CurrentPanel { get; private set; }

	private void Start()
	{
		ShowWelcomePanel();
	}

	private void OnEnable()
	{
		ThirstyEvents.OnWelcome += ShowWelcomePanel;
		ThirstyEvents.OnMorning += ShowMorningPanel;
		ThirstyEvents.OnDay += ShowDayPanel;
		ThirstyEvents.OnNight += ShowNightPanel;
		ThirstyEvents.OnInvest += ShowInvestPanel;
		ThirstyEvents.OnExpand += ShowExpandPanel;
	}

	private void OnDisable()
	{
		ThirstyEvents.OnWelcome -= ShowWelcomePanel;
		ThirstyEvents.OnMorning -= ShowMorningPanel;
		ThirstyEvents.OnDay -= ShowDayPanel;
		ThirstyEvents.OnNight -= ShowNightPanel;
		ThirstyEvents.OnInvest -= ShowInvestPanel;
		ThirstyEvents.OnExpand -= ShowExpandPanel;
	}

	private void ShowWelcomePanel()
	{
		ThirstyEvents.OnClockPaused?.Invoke(true);       // pause clock

		WelcomePanel.gameObject.SetActive(true);
		MorningPanel.gameObject.SetActive(false);
		InventoryPanel.gameObject.SetActive(false);  
		DayPanel.gameObject.SetActive(false);
		NightPanel.gameObject.SetActive(false);
		InvestPanel.gameObject.SetActive(false);
		ExpandPanel.gameObject.SetActive(false);
	}

	private void ShowMorningPanel()
	{
		ThirstyEvents.OnClockPaused?.Invoke(false);		// clock keeps ticking!

		WelcomePanel.gameObject.SetActive(false);
		MorningPanel.gameObject.SetActive(true);
		InventoryPanel.gameObject.SetActive(true);		// to choose drink to sell
		DayPanel.gameObject.SetActive(false);
		NightPanel.gameObject.SetActive(false);
		InvestPanel.gameObject.SetActive(false);
		ExpandPanel.gameObject.SetActive(false);
	}

	private void ShowDayPanel()
	{
		ThirstyEvents.OnClockPaused?.Invoke(false);

		WelcomePanel.gameObject.SetActive(false);
		MorningPanel.gameObject.SetActive(false);
		InventoryPanel.gameObject.SetActive(false);
		DayPanel.gameObject.SetActive(true);
		NightPanel.gameObject.SetActive(false);
		InvestPanel.gameObject.SetActive(false);
		ExpandPanel.gameObject.SetActive(false);
	}

	private void ShowNightPanel()
	{
		ThirstyEvents.OnClockPaused?.Invoke(false);      // clock keeps ticking!

		WelcomePanel.gameObject.SetActive(false);
		MorningPanel.gameObject.SetActive(false);
		InventoryPanel.gameObject.SetActive(true);		// to order more stock
		DayPanel.gameObject.SetActive(false);
		NightPanel.gameObject.SetActive(true);
		InvestPanel.gameObject.SetActive(false);
		ExpandPanel.gameObject.SetActive(false);
	}

	private void ShowInvestPanel()
	{
		ThirstyEvents.OnClockPaused?.Invoke(true);		// pause clock

		WelcomePanel.gameObject.SetActive(false);
		MorningPanel.gameObject.SetActive(false);
		InventoryPanel.gameObject.SetActive(false);
		DayPanel.gameObject.SetActive(false);
		NightPanel.gameObject.SetActive(false);
		InvestPanel.gameObject.SetActive(true);
		ExpandPanel.gameObject.SetActive(false);
	}

	private void ShowUpgradePanel()
	{
		ThirstyEvents.OnClockPaused?.Invoke(true);		// pause clock

		WelcomePanel.gameObject.SetActive(false);
		MorningPanel.gameObject.SetActive(false);
		InventoryPanel.gameObject.SetActive(false);
		DayPanel.gameObject.SetActive(false);
		NightPanel.gameObject.SetActive(false);
		InvestPanel.gameObject.SetActive(false);
		ExpandPanel.gameObject.SetActive(false);
	}

	private void ShowExpandPanel()
	{
		ThirstyEvents.OnClockPaused?.Invoke(true);		// pause clock

		WelcomePanel.gameObject.SetActive(false);
		MorningPanel.gameObject.SetActive(false);
		InventoryPanel.gameObject.SetActive(false);
		DayPanel.gameObject.SetActive(false);
		NightPanel.gameObject.SetActive(false);
		InvestPanel.gameObject.SetActive(false);
		ExpandPanel.gameObject.SetActive(true);
	}



	private void FadeInPanel(Image toPanel, System.Action onBlackout = null, bool fade = true)
	{
		if (!fade)
		{
			SwitchToPanel(toPanel);
		}
		else
		{
			if (blackedOut)
			{
				SwitchToPanel(toPanel);
				ClearBlackout();
			}
			else
			{
				blackOut.color = Color.clear;
				blackOut.gameObject.SetActive(true);

				LeanTween.value(gameObject, FadeColourCallback, Color.clear, blackOutColour, fadeToBlackTime)
							.setEase(LeanTweenType.easeOutCubic)
							.setOnComplete(() =>
							{
								blackedOut = true;

								// call blackout Action if provided
								onBlackout?.Invoke();

								AudioSource.PlayClipAtPoint(FadeInAudio, Vector3.zero);

								// switch panels while blacked-out
								SwitchToPanel(toPanel);
								ClearBlackout();    // tween to clear
							});
			}
		}
	}

	private void ClearBlackout()
	{
		if (!blackedOut)
			return;

		//Force no UI to be selected when we return from a fade
		UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);

		// fade back to clear
		LeanTween.value(gameObject, FadeColourCallback, blackOutColour, Color.clear, fadeToBlackTime)
						.setEase(LeanTweenType.easeInCubic)
						.setOnComplete(() =>
						{
							blackOut.gameObject.SetActive(false);
							blackedOut = false;
						});
	}

	private void FadeColourCallback(Color toColour)
	{
		blackOut.color = toColour;
	}


	private void SwitchToPanel(Image toPanel)
	{
		if (CurrentPanel != null)
			CurrentPanel.gameObject.SetActive(false);

		CurrentPanel = toPanel;
		CurrentPanel.gameObject.SetActive(true);
	}
}
