﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class PulseUI
{
	private const float textScaleTime = 0.1f;
	private const float textScaleFactor = 1.15f;

	private const float pingPongScaleTime = 0.5f;
	private const float pingPongScaleFactor = 2f;

	private static LTDescr textTween;
	private static LTDescr buttonTween;

	private static Vector2 startTextScale;
	private static Vector2 startButtonScale;


	public static void PulseText(Text text, bool pingPong)
	{
		if (textTween != null)
		{
			LeanTween.cancel(textTween.id);
			textTween = null;
		}

		startTextScale = text.transform.localScale;

		if (pingPong)
		{
			StopPulseText(text);

			textTween = LeanTween.scale(text.gameObject, startTextScale * pingPongScaleFactor, pingPongScaleTime)
						.setEase(LeanTweenType.easeOutSine)
						.setLoopPingPong();
		}
		else
		{
			textTween = LeanTween.scale(text.gameObject, startTextScale * textScaleFactor, textScaleTime)
									.setEase(LeanTweenType.easeOutSine)
									.setOnComplete(() =>
									{
										LeanTween.scale(text.gameObject, startTextScale, textScaleTime / 2f)
												.setEase(LeanTweenType.easeInSine)
												.setOnComplete(() =>
												{
													textTween = null;
												});

										text.transform.localScale = startTextScale;
									});
		}
	}

	public static void PulseButton(Button button)
	{
		if (buttonTween != null)
		{
			LeanTween.cancel(buttonTween.id);
			buttonTween = null;
		}

		startButtonScale = button.transform.localScale;

		buttonTween = LeanTween.scale(button.gameObject, startButtonScale * pingPongScaleFactor, pingPongScaleTime)
								.setEase(LeanTweenType.easeOutSine)
								.setLoopPingPong();
								//.setOnComplete(() =>
								//{
								//	LeanTween.scale(button.gameObject, startScale, scaleTime / 2f)
								//			.setEase(LeanTweenType.easeInSine)
								//			.setOnComplete(() =>
								//			{
								//				buttonTween = null;
								//			});

								//	button.transform.localScale = startScale;
								//});
	}

	public static void StopPulseText(Text text)
	{
		if (textTween != null)
		{
			LeanTween.cancel(textTween.id);
			textTween = null;

			text.transform.localScale = startTextScale;
		}
	}

	public static void StopPulseButton(Button button)
	{
		if (buttonTween != null)
		{
			LeanTween.cancel(buttonTween.id);
			buttonTween = null;

			button.transform.localScale = startButtonScale;
		}
	}
}
