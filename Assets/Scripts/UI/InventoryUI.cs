﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour
{
	public InventorySO DrinkInventory;
	public DrinkUI DrinkUIPrefab;

	public Transform ScrollViewContent;

	public Text StockHeader;
	public Text NewDrinkUnlocked;
	public Text TotalOrderValue;
	public Button OrderButton;        // ...end night

	public Image WarningPanel;          // not enough coins to order
	public Text WarningMessage;         // not enough coins to order
	public AudioClip WarningAudio;
	public float WarningTime;			// number of seconds to show warning for

	public BusinessRecords BusinessRecords;

	private int totalOrderValue;
	private Coroutine warningCoroutine;


	private void OnEnable()
	{
		ThirstyEvents.OnDrinkToOrderChanged += OnDrinkToOrderChanged;
		//ThirstyEvents.OnNight += OnNight;
		//ThirstyEvents.OnDrinkUnlocked += OnDrinkUnlocked;

		OrderButton.onClick.AddListener(OnOrderClick);

		CheckUnlockDrink();
	}

	private void OnDisable()
	{
		ThirstyEvents.OnDrinkToOrderChanged -= OnDrinkToOrderChanged;
		//ThirstyEvents.OnNight -= OnNight;
		//ThirstyEvents.OnDrinkUnlocked -= OnDrinkUnlocked;

		OrderButton.onClick.RemoveListener(OnOrderClick);

		HideNewDrink();
	}


	private void Start()
	{
		InitDrinks();
	}

	private void InitDrinks()
	{
		//bool firstDrink = true;

		if (BusinessRecords.CurrentDrink == null)
			BusinessRecords.CurrentDrink = DrinkInventory.Inventory[0];     // select first drink

		ThirstyEvents.OnDrinkSelected?.Invoke(BusinessRecords.CurrentDrink);

		foreach (var drink in DrinkInventory.Inventory)
		{
			var drinkUI = Instantiate(DrinkUIPrefab, ScrollViewContent);

			drinkUI.DrinkName.text = drink.Drink.DrinkName;
			drinkUI.DrinkInventory = drink;

			drinkUI.DrinkSprite.sprite = drink.Drink.DrinkSprite;
			drinkUI.DrinkSprite.color = drink.Drink.DrinkColour;
			drinkUI.ShelfLife.text = drink.Drink.ShelfLife.ToString();
			drinkUI.PackQty.text = drink.Drink.PackQty.ToString();
			drinkUI.Cost.text = drink.Drink.CostPerCup.ToString();

			drinkUI.Stock.text = drink.InStock.ToString();
			drinkUI.Spoilt.text = drink.Spoilt.ToString();
			drinkUI.OnOrder.text = drink.OnOrder.ToString();
			drinkUI.ToOrder.text = drink.ToOrder.ToString();

			drinkUI.CheckLock();

			//ThirstyEvents.OnDrinkSelected?.Invoke(BusinessRecords.CurrentDrink);

			HideNewDrink();
			//if (firstDrink)
			//{
			//	//ThirstyEvents.OnDrinkSelected?.Invoke(drink);

			//	drinkUI.LockDrink(false);
			//	firstDrink = false;
			//}
		}
	}


	private void OnDrinkToOrderChanged(string drinkName, int changeQty, int toOrder, int costPerCup)
	{
		totalOrderValue = toOrder * costPerCup;
		TotalOrderValue.text = totalOrderValue.ToString();
	}

	//private void OnDrinkUnlocked(DrinkInventorySO drink)
	//{
	//	NewDrinkUnlocked.text = drink.Drink.DrinkName + " Unlocked!";
	//	NewDrinkUnlocked.enabled = true;
	//	StockHeader.enabled = false;
	//}

	private void HideNewDrink()
	{
		NewDrinkUnlocked.enabled = false;
		StockHeader.enabled = true;
	}


	// unlock a drink every (eg. 3) profitable days
	//private void OnNight()
	private void CheckUnlockDrink()
	{
		if (BusinessRecords.CurrentHour != DayCycleManager.DayEndHour)		// ie. OnNight
			return;

		if (BusinessRecords.CanUnlockDrink)			// after every (eg. 3) consecutive profitable days
		{
			var unlockDrink = NextDrinkToUnlock();

			if (unlockDrink != null)
			{
				unlockDrink.Drink.Unlocked = true;			// checked by DrinkUI OnEnabled

				NewDrinkUnlocked.text = unlockDrink.Drink.DrinkName + " Unlocked!";
				NewDrinkUnlocked.enabled = true;
				StockHeader.enabled = false;

				ThirstyEvents.OnDrinkUnlocked?.Invoke(unlockDrink);
			}
		}
	}

	private DrinkInventorySO NextDrinkToUnlock()
	{
		foreach (var drink in DrinkInventory.Inventory)
		{
			if (drink.Drink.Unlocked)
				continue;

			return drink;
		}

		return null;
	}

	private void OnOrderClick()
	{
		if (totalOrderValue > BusinessRecords.CurrentCoins)
			ShowWarning("Not Enough Coins!");
		else		// enough coins
			ThirstyEvents.OnPlaceOrder?.Invoke(totalOrderValue);
	}

	private IEnumerator TimedWarningMessage(string message)
	{
		if (string.IsNullOrEmpty(message))
		{
			WarningPanel.gameObject.SetActive(false);
			WarningMessage.text = "";
			yield break;
		}

		WarningPanel.gameObject.SetActive(true);
		WarningMessage.text = message;

		if (WarningAudio != null)
			AudioSource.PlayClipAtPoint(WarningAudio, Vector3.zero);

		yield return new WaitForSeconds(WarningTime);

		WarningMessage.text = "";
		WarningPanel.gameObject.SetActive(false);
		yield return null;
	}

	private void ShowWarning(string message)
	{
		if (warningCoroutine != null)
		{
			StopCoroutine(warningCoroutine);
		}

		warningCoroutine = StartCoroutine(TimedWarningMessage(message));
	}
}
