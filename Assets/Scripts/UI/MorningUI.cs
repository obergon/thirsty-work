﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;


public class MorningUI : MonoBehaviour
{
	public Text DrinkToSell;
	public Image DrinkImage;

	public Button NumCupsUpButton;
	public Text NumCupsUpText;				// pulse
	public Button NumCupsDownButton;
	public Text NumCups;

	public Button PricePerCupUpButton;
	public Button PricePerCupDownButton;
	public Text PricePerCup;

	public Button NumAdsUpButton;
	public Button NumAdsDownButton;
	public Text NumAds;
	public Text CostPerAd;

	public Text Paused;

	public Image WarningPanel;				// not enough stock / not enough coins
	public Text WarningMessage;				// not enough stock / not enough coins
	public AudioClip WarningAudio;       
	public float WarningTime = 3f;			// number of seconds to show warning for

	public Button LeaderboardButton;        //

	public Button TogglePauseButton;
	public Image TogglePlayImage;			// pause / unpause
	public Image TogglePauseImage;          // pause / unpause
	public Text TogglePauseText;            // pause / unpause

	public Button StartDayButton;        // ...end morning

	public int cupsToSellMultiple = 10;

	private int totalOrderValue = 0;		// to enable start day button

	public BusinessRecords BusinessRecords;         // SO

	private Coroutine warningCoroutine;

	private const float cupsUpScaleFactor = 2f;
	private const float cupsUpScaleTime = 0.5f;
	private LTDescr cupsUpTween;
	private Vector2 startCupsUpScale;


	private void OnEnable()
	{
		ThirstyEvents.OnPlaceOrder += OnPlaceOrder;
		ThirstyEvents.OnClockPaused += OnClockPaused;
		ThirstyEvents.OnDrinkSelected += OnDrinkSelected;

		NumCupsUpButton.onClick.AddListener(OnNumCupsUpClick);
		NumCupsDownButton.onClick.AddListener(OnNumCupsDownClick);

		PricePerCupUpButton.onClick.AddListener(OnPricePerCupUpClick);
		PricePerCupDownButton.onClick.AddListener(OnPricePerCupDownClick);

		NumAdsUpButton.onClick.AddListener(OnNumAdsUpClick);
		NumAdsDownButton.onClick.AddListener(OnNumAdsDownClick);

		LeaderboardButton.onClick.AddListener(OnLeaderboardClick);
		StartDayButton.onClick.AddListener(OnStartDayClick);

		TogglePauseButton.onClick.AddListener(OnTogglePauseClick);

		InitStrategy();
	}

	private void OnDisable()
	{
		ThirstyEvents.OnPlaceOrder -= OnPlaceOrder;
		ThirstyEvents.OnClockPaused -= OnClockPaused;
		ThirstyEvents.OnDrinkSelected -= OnDrinkSelected;

		NumCupsUpButton.onClick.RemoveListener(OnNumCupsUpClick);
		NumCupsDownButton.onClick.RemoveListener(OnNumCupsDownClick);

		PricePerCupUpButton.onClick.RemoveListener(OnPricePerCupUpClick);
		PricePerCupDownButton.onClick.RemoveListener(OnPricePerCupDownClick);

		NumAdsUpButton.onClick.RemoveListener(OnNumAdsUpClick);
		NumAdsDownButton.onClick.RemoveListener(OnNumAdsDownClick);

		LeaderboardButton.onClick.RemoveListener(OnLeaderboardClick);
		StartDayButton.onClick.RemoveListener(OnStartDayClick);

		TogglePauseButton.onClick.RemoveListener(OnTogglePauseClick);
	}

	private void SetPricePerCup()
	{
		PricePerCup.text = (BusinessRecords.PricePerCup > 0) ? BusinessRecords.PricePerCup.ToString() : "Free";
	}

	private IEnumerator TimedWarningMessage(string message)
	{
		if (string.IsNullOrEmpty(message))
		{
			WarningPanel.gameObject.SetActive(false);
			WarningMessage.text = "";
			yield break;
		}

		WarningPanel.gameObject.SetActive(true);
		WarningMessage.text = message;

		if (WarningAudio != null)
			AudioSource.PlayClipAtPoint(WarningAudio, Vector3.zero);

		yield return new WaitForSeconds(WarningTime);

		WarningMessage.text = "";
		WarningPanel.gameObject.SetActive(false);
		yield return null;
	}

	private void ShowWarning(string message)
	{
		if (warningCoroutine != null)
		{
			StopCoroutine(warningCoroutine);
		}

		warningCoroutine = StartCoroutine(TimedWarningMessage(message));
	}


	private void OnNumCupsUpClick()
	{
		//StopPulseNumCupsUp();

		if (BusinessRecords.CurrentDrink.InStock <= 0)      // no stock!
		{
			ShowWarning("Out of Stock!");
			return;
		}

		// if stock level below cupsToSellMultiple, increment is single numbers
		int cupIncrement = (BusinessRecords.CurrentDrink.InStock < cupsToSellMultiple) ? 1 : cupsToSellMultiple;

		if (BusinessRecords.CurrentDrink.InStock < cupIncrement)      // not enough stock!
		{
			ShowWarning("Not Enough Stock!");
			return;
		}
		if (BusinessRecords.CurrentCoins < (cupIncrement * BusinessRecords.CurrentDrink.Drink.CostPerCup))   // not enough coins!
		{
			ShowWarning("Not Enough Coins!");
			return;
		}

		ShowWarning("");

		IncrementNumCups(cupIncrement);
	}

	private void IncrementNumCups(int cupIncrement)
	{
		BusinessRecords.DayCupsToSell += cupIncrement;

		NumCups.text = BusinessRecords.DayCupsToSell.ToString();
		ThirstyEvents.OnDayCupsToSell?.Invoke(cupIncrement, BusinessRecords.DayCupsToSell);

		EnableStartDayButton();
	}

	private void OnNumCupsDownClick()
	{
		ShowWarning("");
		if (BusinessRecords.DayCupsToSell <= 0)
		{
			BusinessRecords.DayCupsToSell = 0;
			//PulseNumCupsUp();
			return;
		}

		int cupIncrement = (BusinessRecords.DayCupsToSell < cupsToSellMultiple) ? 1 : cupsToSellMultiple;

		BusinessRecords.DayCupsToSell -= cupIncrement;

		if (BusinessRecords.DayCupsToSell <= 0)
		{
			BusinessRecords.DayCupsToSell = 0;
			//PulseNumCupsUp();
		}

		NumCups.text = BusinessRecords.DayCupsToSell.ToString();
		ThirstyEvents.OnDayCupsToSell?.Invoke(-cupIncrement, BusinessRecords.DayCupsToSell);

		EnableStartDayButton();
	}

	private void OnPricePerCupUpClick()
	{
		BusinessRecords.PricePerCup++;

		SetPricePerCup();

		ThirstyEvents.OnPricePerCupChanged?.Invoke(BusinessRecords.PricePerCup);
	}

	private void OnPricePerCupDownClick()
	{
		BusinessRecords.PricePerCup--;

		if (BusinessRecords.PricePerCup <= 0)
			BusinessRecords.PricePerCup = 0;

		SetPricePerCup();
		ThirstyEvents.OnPricePerCupChanged?.Invoke(BusinessRecords.PricePerCup);
	}

	private void OnNumAdsUpClick()
	{
		if (BusinessRecords.CurrentCoins < BusinessRecords.CostPerAd)   // not enough coins!
		{
			ShowWarning("Not Enough Coins!");
		}
		else
		{
			IncrementNumAdPosters(1);
		}
	}

	private void IncrementNumAdPosters(int adIncrement)
	{
		BusinessRecords.DayAdPosters += adIncrement;

		NumAds.text = BusinessRecords.DayAdPosters.ToString();
		ThirstyEvents.OnDayAdPosters?.Invoke(adIncrement, BusinessRecords.DayAdPosters);
	}

	private void OnNumAdsDownClick()
	{
		if (BusinessRecords.DayAdPosters == 0)
			return;

		ShowWarning("");
		BusinessRecords.DayAdPosters--;

		if (BusinessRecords.DayAdPosters <= 0)
			BusinessRecords.DayAdPosters = 0;

		NumAds.text = BusinessRecords.DayAdPosters.ToString();
		ThirstyEvents.OnDayAdPosters?.Invoke(-1, BusinessRecords.DayAdPosters);
	}


	private void OnClockPaused(bool paused)
	{
		TogglePauseText.text = paused ? "Unpause" : "Pause";
		TogglePlayImage.enabled = paused;
		TogglePauseImage.enabled = !paused;

		Paused.enabled = paused;
	}

	private void OnPlaceOrder(int totalValue)
	{
		totalOrderValue = totalValue;
		EnableStartDayButton();
	}

	private void OnLeaderboardClick()
	{
		ThirstyEvents.OnLeaderboard?.Invoke();
	}

	private void OnStartDayClick()
	{
		ThirstyEvents.OnDay?.Invoke();
	}

	private void OnTogglePauseClick()
	{
		ThirstyEvents.OnClockTogglePause?.Invoke();
	}

	private void OnDrinkSelected(DrinkInventorySO drink)
	{
		DrinkToSell.text = drink.Drink.DrinkName;
		DrinkImage.sprite = drink.Drink.DrinkSprite;
		DrinkImage.color = drink.Drink.DrinkColour;

		if (BusinessRecords.PlayCount == 0)
		{
			IncrementNumCups(cupsToSellMultiple);           // init cups to sell for first play
			IncrementNumAdPosters(1);						// init ad posters for first play
		}
	}

	//private void PulseNumCupsUp()
	//{
	//	if (cupsUpTween != null)
	//	{
	//		LeanTween.cancel(cupsUpTween.id);
	//		cupsUpTween = null;
	//	}

	//	startCupsUpScale = NumCupsUpText.transform.localScale;

	//	StopPulseNumCupsUp();

	//	cupsUpTween = LeanTween.scale(NumCupsUpText.gameObject, startCupsUpScale * cupsUpScaleFactor, cupsUpScaleTime)
	//							.setEase(LeanTweenType.easeOutSine)
	//							.setLoopPingPong();
	//}

	//private void StopPulseNumCupsUp()
	//{
	//	if (cupsUpTween != null)
	//	{
	//		LeanTween.cancel(cupsUpTween.id);
	//		cupsUpTween = null;

	//		NumCupsUpText.transform.localScale = startCupsUpScale;
	//	}
	//}

	private void EnableStartDayButton()
	{
		StartDayButton.interactable = BusinessRecords.DayCupsToSell > 0 || totalOrderValue > 0;
	}

	private void InitStrategy()
	{
		BusinessRecords.DayCupsToSell = 0;
		BusinessRecords.DayAdPosters = 0;
		BusinessRecords.PricePerCup = 0;

		NumCups.text = "0";
		SetPricePerCup();
		NumAds.text = "0";

		StartDayButton.interactable = false;

		//PulseNumCupsUp();
	}
}