﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NightUI : MonoBehaviour
{
	public Text DayOpening;
	public Text DaySales;
	public Text DayCosts;
	public Text DaySpoilage;
	public Text DayClosing;
	public Text DayProfitLoss;

	public Text Paused;

	public Button TogglePauseButton;
	public Image TogglePlayImage;           // pause / unpause
	public Image TogglePauseImage;          // pause / unpause
	public Text TogglePauseText;            // pause / unpause

	public Button MorningButton;		// ...end night
	public Button InvestButton;         //
	public Button FriendsButton;        //

	public BusinessRecords BusinessRecords;			// SO


	private void OnEnable()
	{
		ThirstyEvents.OnClockPaused += OnClockPaused;

		MorningButton.onClick.AddListener(OnMorningClick);
		InvestButton.onClick.AddListener(OnInvestClick);
		FriendsButton.onClick.AddListener(OnFriendsClick);
		TogglePauseButton.onClick.AddListener(OnTogglePauseClick);

		DayClosingStats();
	}

	private void OnDisable()
	{
		ThirstyEvents.OnClockPaused += OnClockPaused;

		MorningButton.onClick.RemoveListener(OnMorningClick);
		InvestButton.onClick.RemoveListener(OnInvestClick);
		FriendsButton.onClick.RemoveListener(OnFriendsClick);
		TogglePauseButton.onClick.RemoveListener(OnTogglePauseClick);
	}

	private void OnMorningClick()
	{
		ThirstyEvents.OnMorning?.Invoke();      // morning panel (strategy) - no change of time
	}

	private void OnInvestClick()
	{
		ThirstyEvents.OnInvest?.Invoke();

	}

	private void OnFriendsClick()
	{
		ThirstyEvents.OnFriends?.Invoke();
	}


	private void OnTogglePauseClick()
	{
		ThirstyEvents.OnClockTogglePause?.Invoke();
	}

	private void OnClockPaused(bool paused)
	{
		TogglePauseText.text = paused ? "Unpause" : "Pause";
		TogglePlayImage.enabled = paused;
		TogglePauseImage.enabled = !paused;

		Paused.enabled = paused;
	}


	private void DayClosingStats()
	{
		DayOpening.text = BusinessRecords.DayOpeningCoins.ToString();

		DaySales.text = BusinessRecords.DayCupSales.ToString();
		DayCosts.text = BusinessRecords.DayCupCost.ToString();
		DaySpoilage.text = "tba";        // TODO

		DayClosing.text = BusinessRecords.CurrentCoins.ToString();

		var profitLoss = BusinessRecords.DayCupSales - BusinessRecords.DayCupCost;
		DayProfitLoss.text = profitLoss.ToString();

		if (profitLoss > 0)
		{
			BusinessRecords.ProfitableDayCount++;
			BusinessRecords.ConsecutiveProfitableDays++;
		}
		else
		{
			BusinessRecords.ConsecutiveProfitableDays = 0;
		}
	}
}
