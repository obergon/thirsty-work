﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameUI : MonoBehaviour
{
	public Text Coins;
	public Text DayStartCoins;
	public Text Hour;
	public Text DayCount;
	//public Text Paused;

	public Image RandomWeatherEventPanel;
	public Image RandomWeatherEventImage;
	public Text RandomWeatherEventText;
	public Button RandomWeatherEventOkButton;

	public Image RandomNewsEventPanel;
	public Image RandomNewsEventImage;
	public Text RandomNewsEventText;
	public Button RandomNewsEventOkButton;

	public Image BankruptPanel;
	public Button BankruptOkButton;

	public Image SoldOutPanel;
	public Button SoldOutOkButton;

	public AudioClip CoinsAudio;
	public AudioClip NewsAudio;
	public AudioClip SoldOutAudio;
	public AudioClip BankruptAudio;

	public List<WeatherDayUI> WeatherForecast;

	public float PopupTime = 10f;

	public BusinessRecords BusinessRecords;         // SO

	public enum PopupType
	{
		Weather,
		News,
		SoldOut,
		Bankrupt
	}

	private void OnEnable()
	{
		ThirstyEvents.OnHour += OnHour;
		ThirstyEvents.OnCoinsChanged += OnCoinsChanged;
		ThirstyEvents.OnMorning += OnMorning;
		ThirstyEvents.OnDayCount += OnDayCount;
		ThirstyEvents.OnClockPaused += OnClockPaused;

		ThirstyEvents.OnRandomWeatherEvent += OnRandomWeatherEvent;
		ThirstyEvents.OnRandomNewsEvent += OnRandomNewsEvent;
		ThirstyEvents.OnSoldOut += OnSoldOut;
		ThirstyEvents.OnBankrupted += OnBankrupted;

		RandomWeatherEventOkButton.onClick.AddListener(OnRandomWeatherEventOk);
		RandomNewsEventOkButton.onClick.AddListener(OnRandomNewsEventOk);

		SoldOutOkButton.onClick.AddListener(OnSoldOutOk);
		BankruptOkButton.onClick.AddListener(OnBankruptOk);

		OnCoinsChanged(BusinessRecords.CurrentCoins);
		SetHour(BusinessRecords.CurrentHour);
		OnDayCount(BusinessRecords.DayCount, false);
	}

	private void OnDisable()
	{
		ThirstyEvents.OnHour -= OnHour;
		ThirstyEvents.OnCoinsChanged -= OnCoinsChanged;
		ThirstyEvents.OnMorning -= OnMorning;
		ThirstyEvents.OnDayCount -= OnDayCount;
		ThirstyEvents.OnClockPaused -= OnClockPaused;

		ThirstyEvents.OnRandomWeatherEvent -= OnRandomWeatherEvent;
		ThirstyEvents.OnRandomNewsEvent -= OnRandomNewsEvent;
		ThirstyEvents.OnSoldOut -= OnSoldOut;
		ThirstyEvents.OnBankrupted -= OnBankrupted;

		RandomWeatherEventOkButton.onClick.RemoveListener(OnRandomWeatherEventOk);
		RandomNewsEventOkButton.onClick.RemoveListener(OnRandomNewsEventOk);

		SoldOutOkButton.onClick.RemoveListener(OnSoldOutOk);
		BankruptOkButton.onClick.RemoveListener(OnBankruptOk);
	}

	private Coroutine weatherPopupCoroutine;
	private Coroutine newsPopupCoroutine;
	private Coroutine soldOutPopupCoroutine;
	private Coroutine bankruptPopupCoroutine;


	private void OnHour(int dayCount, int HourCount, int hour, int dayStartHour, int dayEndHour, float clockTickInterval)
	{
		SetHour(hour);
	}

	private void SetHour(int hour)
	{
		Hour.text = ((hour < 10) ? "0" : "") + hour + ":00";
	}

	private void OnDayCount(int day, bool startUp)
	{
		DayCount.text = "Day " + day;
	}

	// save starting coins before player spends some
	private void OnMorning()
	{
		//if (BusinessRecords.DayCount > 1)
		DayStartCoins.text = Coins.text;
		//else
		//	DayStartCoins.text = "";
	}

	private void OnClockPaused(bool paused)
	{
		//Paused.enabled = paused;
		Hour.enabled = !paused;
		DayCount.enabled = !paused;
	}

	private void OnCoinsChanged(int coins)
	{
		Coins.text = string.Format("{0:N0}", coins);        // comma separated

		PulseUI.PulseText(Coins, false);

		if (CoinsAudio != null)
			AudioSource.PlayClipAtPoint(CoinsAudio, Vector3.zero, 10f);
	}


	private void OnRandomWeatherEvent(WeatherCondition randomWeather)
	{
		if (randomWeather.Weather.WeatherSprite != null)
			RandomWeatherEventImage.sprite = randomWeather.Weather.WeatherSprite;

		//RandomWeatherEventSprite.color = randomWeather.Weather.WeatherColour;
		RandomWeatherEventText.text = randomWeather.WeatherDescription();  // randomWeather.Weather.WeatherName + "!!";

		ShowPopup(PopupType.Weather);
	}

	private void OnRandomWeatherEventOk()
	{
		HidePopup(PopupType.Weather);
	}


	private void OnRandomNewsEvent()
	{
		ShowPopup(PopupType.News);

		if (NewsAudio != null)
			AudioSource.PlayClipAtPoint(NewsAudio, Vector3.zero, 10f);
	}

	private void OnRandomNewsEventOk()
	{
		HidePopup(PopupType.News);
	}


	private void OnBankrupted(int coins)
	{
		ShowPopup(PopupType.Bankrupt);

		if (BankruptAudio != null)
			AudioSource.PlayClipAtPoint(BankruptAudio, Vector3.zero, 10f);
	}

	private void OnBankruptOk()
	{
		HidePopup(PopupType.Bankrupt);
	}


	private void OnSoldOut()
	{
		ShowPopup(PopupType.SoldOut);

		if (SoldOutAudio != null)
			AudioSource.PlayClipAtPoint(SoldOutAudio, Vector3.zero, 10f);
	}

	private void OnSoldOutOk()
	{
		HidePopup(PopupType.SoldOut);
		ThirstyEvents.OnNight?.Invoke();
	}


	private void ShowPopup(PopupType type)
	{
		HidePopup(type);    // make sure coroutine not aready running (shouldn't be...)		

		switch (type)
		{
			case PopupType.Weather:
				StartCoroutine(WeatherTimedPopup());
				break;

			case PopupType.News:
				StartCoroutine(NewsTimedPopup());
				break;

			case PopupType.SoldOut:
				StartCoroutine(SoldOutTimedPopup());
				break;

			case PopupType.Bankrupt:
				StartCoroutine(BankruptTimedPopup());
				break;
		}
	}

	private IEnumerator WeatherTimedPopup()
	{
		RandomWeatherEventPanel.gameObject.SetActive(true);
		yield return new WaitForSeconds(PopupTime);
		HidePopup(PopupType.Weather);
	}

	private IEnumerator NewsTimedPopup()
	{
		RandomNewsEventPanel.gameObject.SetActive(true);
		yield return new WaitForSeconds(PopupTime);
		HidePopup(PopupType.News);
	}

	private IEnumerator SoldOutTimedPopup()
	{
		SoldOutPanel.gameObject.SetActive(true);
		yield return new WaitForSeconds(PopupTime);
		HidePopup(PopupType.SoldOut);
	}

	private IEnumerator BankruptTimedPopup()
	{
		BankruptPanel.gameObject.SetActive(true);
		yield return new WaitForSeconds(PopupTime);
		HidePopup(PopupType.Bankrupt);
	}

	private void HidePopup(PopupType type)
	{
		switch (type)
		{
			case PopupType.Weather:
				if (weatherPopupCoroutine != null)
				{
					StopCoroutine(weatherPopupCoroutine);
					weatherPopupCoroutine = null;
				}
				RandomWeatherEventPanel.gameObject.SetActive(false);
				break;

			case PopupType.News:
				if (newsPopupCoroutine != null)
				{
					StopCoroutine(newsPopupCoroutine);
					newsPopupCoroutine = null;
				}
				RandomNewsEventPanel.gameObject.SetActive(false);
				break;

			case PopupType.SoldOut:
				if (soldOutPopupCoroutine != null)
				{
					StopCoroutine(soldOutPopupCoroutine);
					soldOutPopupCoroutine = null;
				}
				SoldOutPanel.gameObject.SetActive(false);
				break;

			case PopupType.Bankrupt:
				if (bankruptPopupCoroutine != null)
				{
					StopCoroutine(bankruptPopupCoroutine);
					bankruptPopupCoroutine = null;
				}
				BankruptPanel.gameObject.SetActive(false);
				break;
		}
	}
}