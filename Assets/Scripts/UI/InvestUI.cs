﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_ADS
using UnityEngine.Advertisements;
#endif


public class InvestUI : MonoBehaviour
{
	public Button IAPButton;
	public Button AdButton;

	public Sprite Stall1Sprite;
	public Sprite Stall2Sprite;
	public Sprite Stall3Sprite;
	public Image CurrentStallImage;

	public Button Stall2Button;
	public Button Stall3Button;

	public Button ConfirmUpgradeButton;
	public Text ConfirmCoinsText;

	public Button ContinueButton;        // ...return to night

	public int Stall2Coins;
	public int Stall3Coins;

	public BusinessRecords BusinessRecords;

	private int stallLevelSelected;
	private int stallLevelCoins;

	public AdManager AdManager;


	private void OnEnable()
	{
		IAPButton.onClick.AddListener(OnIAPButtonClick);
		AdButton.onClick.AddListener(OnAdButtonClick);

		Stall2Button.onClick.AddListener(OnStall2ButtonClick);
		Stall3Button.onClick.AddListener(OnStall3ButtonClick);
		ConfirmUpgradeButton.onClick.AddListener(OnConfirmClick);
		ContinueButton.onClick.AddListener(OnContinueClick);

		InitSelectedStall();

		ConfirmUpgradeButton.interactable = false;
		ConfirmCoinsText.text = "Confirm";
	}

	private void OnDisable()
	{
		IAPButton.onClick.RemoveListener(OnIAPButtonClick);
		AdButton.onClick.RemoveListener(OnAdButtonClick);

		Stall2Button.onClick.RemoveListener(OnStall2ButtonClick);
		Stall3Button.onClick.RemoveListener(OnStall3ButtonClick);
		ConfirmUpgradeButton.onClick.RemoveListener(OnConfirmClick);
		ContinueButton.onClick.RemoveListener(OnContinueClick);
	}

	private void InitSelectedStall()
	{
		stallLevelSelected = BusinessRecords.CurrentStallLevel;
		stallLevelCoins = 0;

		SetCurrentStallSprite();
		InitStallButtons();
	}

	private void SetCurrentStallSprite()
	{
		if (stallLevelSelected == 2)
			CurrentStallImage.sprite = Stall2Sprite;
		else if (stallLevelSelected == 3)
			CurrentStallImage.sprite = Stall3Sprite;
		else
			CurrentStallImage.sprite = Stall1Sprite;
	}

	private void InitStallButtons()
	{
		Stall2Button.interactable = (stallLevelSelected == 1 && BusinessRecords.CurrentCoins >= Stall2Coins);
		Stall3Button.interactable = (stallLevelSelected == 2 && BusinessRecords.CurrentCoins >= Stall3Coins);
	}

	private void OnStall2ButtonClick()
	{
		stallLevelSelected = 2;
		stallLevelCoins = Stall2Coins;

		ConfirmCoinsText.text = "Confirm " + string.Format("{0:N0}...", stallLevelCoins);
		ConfirmUpgradeButton.interactable = true;
	}

	private void OnStall3ButtonClick()
	{
		stallLevelSelected = 3;
		stallLevelCoins = Stall3Coins;

		ConfirmCoinsText.text = "Confirm " + string.Format("{0:N0}...", stallLevelCoins);
		ConfirmUpgradeButton.interactable = true;
	}

	private void OnConfirmClick()
	{
		ConfirmUpgradeButton.interactable = false;
		ConfirmCoinsText.text = string.Format("{0:N0} Confirmed!", stallLevelCoins);

		ThirstyEvents.OnStallUpgrade?.Invoke(stallLevelSelected, stallLevelCoins);

		InitSelectedStall();
	}


	private void OnIAPButtonClick()
	{
		// TODO
	}

	private void OnAdButtonClick()
	{
		AdManager.ShowRewardedAd(OnRewardedAdClosed);
	}

	private void OnRewardedAdClosed(ShowResult result)
	{
		Debug.Log("Rewarded ad closed");

		switch (result)
		{
			case ShowResult.Finished:
				Debug.Log("Ad finished - reward player!");
				ThirstyEvents.OnAdWatched?.Invoke();
				break;

			case ShowResult.Failed:
				Debug.Log("Ad failed");
				break;

			case ShowResult.Skipped:
				Debug.Log("Ad skipped - no reward!");
				break;
		}

	}

	private void OnContinueClick()
	{
		ThirstyEvents.OnNight?.Invoke();
	}
}
