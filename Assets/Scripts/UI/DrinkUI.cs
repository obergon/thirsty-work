﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrinkUI : MonoBehaviour
{
	public DrinkInventorySO DrinkInventory;

	public Text DrinkName;
	public Image DrinkSprite;
	public Text Stock;
	public Text Spoilt;
	public Text Cost;
	public Text ShelfLife;
	public Text OnOrder;
	public Text PackQty;
	public Text ToOrder;
	public Image DrinkLock;


	public Button UpToOrderButton;
	public Button DownToOrderButton;


	private void OnEnable()
	{
		ThirstyEvents.OnDrinkStockChanged += OnDrinkStockChanged;
		ThirstyEvents.OnDrinkSpoiltChanged += OnDrinkSpoiltChanged;
		ThirstyEvents.OnDrinkOnOrderChanged += OnDrinkOnOrderChanged;
		ThirstyEvents.OnDrinkToOrderChanged += OnDrinkToOrderChanged;
		ThirstyEvents.OnDrinkStockIssued += OnDrinkIssued;
		//ThirstyEvents.OnDrinkUnlocked += OnDrinkUnlocked;

		UpToOrderButton.onClick.AddListener(IncrementToOrder);
		DownToOrderButton.onClick.AddListener(DecrementToOrder);

		GetComponent<Button>().onClick.AddListener(OnDrinkClicked);

		CheckLock();
	}

	private void OnDisable()
	{
		ThirstyEvents.OnDrinkStockChanged -= OnDrinkStockChanged;
		ThirstyEvents.OnDrinkSpoiltChanged -= OnDrinkSpoiltChanged;
		ThirstyEvents.OnDrinkOnOrderChanged -= OnDrinkOnOrderChanged;
		ThirstyEvents.OnDrinkToOrderChanged -= OnDrinkToOrderChanged;
		ThirstyEvents.OnDrinkStockIssued -= OnDrinkIssued;
		//ThirstyEvents.OnDrinkUnlocked -= OnDrinkUnlocked;

		UpToOrderButton.onClick.RemoveListener(IncrementToOrder);
		DownToOrderButton.onClick.RemoveListener(DecrementToOrder);

		GetComponent<Button>().onClick.RemoveListener(OnDrinkClicked);
	}

	private void OnDrinkStockChanged(string drinkName, int changeQty, int inStock)
	{
		if (DrinkInventory.Drink.DrinkName == drinkName)
			Stock.text = inStock.ToString();
	}

	private void OnDrinkSpoiltChanged(string drinkName, int changeQty, int spoiltQty)
	{
		if (DrinkInventory.Drink.DrinkName == drinkName)
			Spoilt.text = spoiltQty.ToString();
	}

	private void OnDrinkOnOrderChanged(string drinkName, int changeQty, int onOrder, int costPerCup)
	{
		if (DrinkInventory.Drink.DrinkName == drinkName)
			OnOrder.text = onOrder.ToString();
	}

	private void OnDrinkToOrderChanged(string drinkName, int changeQty, int toOrder, int costPerCup)
	{
		if (DrinkInventory.Drink.DrinkName == drinkName)
			ToOrder.text = toOrder.ToString();
	}

	private void OnDrinkIssued(string drinkName, int changeQty, int issueQty)
	{
		if (DrinkInventory.Drink.DrinkName == drinkName)
		{
			var newStockQty = DrinkInventory.IssueStock(changeQty);
			Stock.text = newStockQty.ToString();
		}
	}


	//private void OnDrinkUnlocked(DrinkInventorySO drink)
	//{
	//	if (DrinkInventory.Drink.DrinkName == drink.Drink.DrinkName)
	//	{
	//		LockDrink(false);
	//	}
	//}

	public void LockDrink(bool locked)
	{
		DrinkInventory.Drink.Unlocked = !locked;
		CheckLock();
	}

	public void CheckLock()
	{
		var unlocked = DrinkInventory.Drink.Unlocked;

		GetComponent<Button>().interactable = unlocked;
		DrinkLock.enabled = !unlocked;
		//DrinkSprite.enabled = unlocked;

		UpToOrderButton.interactable = unlocked;
		DownToOrderButton.interactable = unlocked;

		Stock.enabled = unlocked;
		Spoilt.enabled = unlocked;
		Cost.enabled = unlocked;
		ShelfLife.enabled = unlocked;
		OnOrder.enabled = unlocked;
		PackQty.enabled = unlocked;
		ToOrder.enabled = unlocked;

		UpToOrderButton.gameObject.SetActive(unlocked);
		DownToOrderButton.gameObject.SetActive(unlocked);
	}


	// +/- buttons
	private void IncrementToOrder()
	{
		DrinkInventory.UpdateToOrderQty(DrinkInventory.Drink.PackQty);
	}

	private void DecrementToOrder()
	{
		DrinkInventory.UpdateToOrderQty(-DrinkInventory.Drink.PackQty);
	}


	private void OnDrinkClicked()
	{
		ThirstyEvents.OnDrinkSelected?.Invoke(DrinkInventory);
	}
}