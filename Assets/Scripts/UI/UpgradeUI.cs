﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeUI : MonoBehaviour
{
	public Button ContinueButton;        // ...back to night


	private void OnEnable()
	{
		ContinueButton.onClick.AddListener(OnContinueClick);
	}

	private void OnDisable()
	{
		ContinueButton.onClick.RemoveListener(OnContinueClick);
	}


	private void OnContinueClick()
	{
		ThirstyEvents.OnNight?.Invoke();
	}
}
