﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class AnalyticsManager : MonoBehaviour
{
	private AnalyticsEventTracker eventTracker;

	private void Start()
	{
		eventTracker = GetComponent<AnalyticsEventTracker>();
	}

	private void OnEnable()
	{
		ThirstyEvents.OnDailySalesRecord += OnDailySalesRecord;
	}

	private void OnDisable()
	{
		ThirstyEvents.OnDailySalesRecord -= OnDailySalesRecord;
	}
	
	private void OnDailySalesRecord(HourlyRecord dailyRecord)
	{
		Analytics.CustomEvent("DailyRecord", new Dictionary<string, object>
		{
			{ "Day " + dailyRecord.DayNumber.ToString(), dailyRecord.ToJson() }
		});
	}
}
