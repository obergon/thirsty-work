﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeatherDayUI : MonoBehaviour
{
	public WeatherCondition weatherCondition; //{ get; private set; }

	public Image Background;
	public Image WeatherImage;
	public Text DayNum;
	public Text Temp;
	public Text WeatherName;

	public int dayIndex;        // 0 == today


	private void OnEnable()
	{
		ThirstyEvents.OnWeatherChanged += OnWeatherChanged;
		ThirstyEvents.OnRandomWeatherEvent += OnRandomWeatherEvent;
	}

	private void OnDisable()
	{
		ThirstyEvents.OnWeatherChanged -= OnWeatherChanged;
		ThirstyEvents.OnRandomWeatherEvent -= OnRandomWeatherEvent;
	}

	private void OnWeatherChanged(int dayNum, int forecastIndex, WeatherCondition weatherCondition)
	{
		if (dayIndex == forecastIndex)
		{
			SetWeatherCondition(dayNum, weatherCondition);
		}
	}

	private void SetWeatherCondition(int dayNum, WeatherCondition weather)
	{
		weatherCondition = weather;

		//Background.color = weather.Weather.WeatherColour;
		if (weather.Weather.WeatherSprite != null)
			WeatherImage.sprite = weather.Weather.WeatherSprite;

		WeatherImage.color = weather.Weather.WeatherColour;
		DayNum.text = dayNum.ToString();
		Temp.text = weather.Temperature.ToString();
		WeatherName.text = weather.Weather.WeatherName;
	}

	// change today's weather to random weather update
	private void OnRandomWeatherEvent(WeatherCondition randomWeather)
	{
		SetWeatherCondition(0, weatherCondition);
	}
}
