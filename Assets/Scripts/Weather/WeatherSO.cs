﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu(fileName = "Weather", menuName = "Data/Weather/WeatherSO")]
public class WeatherSO : ScriptableObject
{
    public string WeatherName;
    public Color WeatherColour;
    public Sprite WeatherSprite;

	public AudioClip MusicClip;

    public WeatherController.WeatherType WeatherType;

	[Range(0,1)]
	public float OutdoorsFactor;	// how likely are people to go out to the park?
	[Range(0, 1)]
	public float ThirstyFactor;		// how thirsty are people likely to be?

	[Range(0, 1)]
	public float ColdDrinkFactor;	// cold drink weather?
	[Range(0, 1)]
	public float HotDrinkFactor;	// hot drink weather?

	public int MinDayTemp;
	public int MaxDayTemp;

	public int MinNightTemp;
	public int MaxNightTemp;
}
