﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WeatherController : MonoBehaviour
{
	public enum WeatherType
	{
		Fine,
		Windy,
		Rainy,
		Stormy,
		Heatwave
	}

	public List<WeatherSO> Weathers;                // all possible weathers

	public float RandomEventProbability;			// percentage
	public int EventFreeDays;						// first few days no random events
	public int MinHoursSinceLast;                   // to prevent too many weather events near each other

	public BusinessRecords BusinessRecords;			// contains weather forecast (list of WeatherConditions) - eg. 5 days

	private int lastWeatherHourCount = 0;

	public WeatherCondition CurrentWeather { get { return BusinessRecords.WeatherForecast[0]; } }
	private WeatherSO RandomWeather { get { return Weathers[UnityEngine.Random.Range(0, Weathers.Count - 1)]; } }


	private void Start()
	{
		InitWeather();
	}

	private void OnEnable()
	{
		ThirstyEvents.OnHour += OnHour;
		ThirstyEvents.OnDayCount += OnDayCount;
		ThirstyEvents.OnResetData += OnResetData;
	}

	private void OnDisable()
	{
		ThirstyEvents.OnHour -= OnHour;
		ThirstyEvents.OnDayCount -= OnDayCount;
		ThirstyEvents.OnResetData -= OnResetData;
	}


	// sync UI etc.
	private void InitWeather()
	{
		for (int i = 0; i < BusinessRecords.WeatherForecast.Count; i++)
		{
			ThirstyEvents.OnWeatherChanged?.Invoke(BusinessRecords.CurrentDay + i, i, BusinessRecords.WeatherForecast[i]);
		}
	}

	private void OnHour(int dayCount, int hourCount, int hour, int dayStartHour, int dayEndHour, float clockTickInterval)
	{
		// weather conditions change randomly during daytime only
		var canDoEvent = (BusinessRecords.CurrentDay > EventFreeDays)
							&& (hour > dayStartHour) && (hour < dayEndHour)
							&& (hourCount - lastWeatherHourCount) >= MinHoursSinceLast;

		if (canDoEvent && (UnityEngine.Random.Range(0f, 1f) < RandomEventProbability))      // TODO: fancy algorithm!
		{
			var randomWeather = RandomWeather;

			if (RandomWeather.WeatherType != CurrentWeather.Weather.WeatherType)
			{
				var randomTemp = UnityEngine.Random.Range(randomWeather.MinDayTemp, randomWeather.MaxDayTemp);

				CurrentWeather.SetWeather(randomWeather, randomTemp);
				lastWeatherHourCount = hourCount;
			}

			AudioSource.PlayClipAtPoint(randomWeather.MusicClip, Vector3.zero, 10f);
			ThirstyEvents.OnRandomWeatherEvent?.Invoke(CurrentWeather);
		}
	}

	private void OnDayCount(int dayNum, bool startUp)
	{
		if (!startUp)
			ShuffleWeather(dayNum);
	}

	// at midnight, move tomorrow's forecast to today, etc.
	private void ShuffleWeather(int dayNum)
	{
		for (int i = 0; i < BusinessRecords.WeatherForecast.Count - 1; i++)
		{
			var dayForecast = BusinessRecords.WeatherForecast[i];
			var nextDayForecast = BusinessRecords.WeatherForecast[i + 1];

			dayForecast.SetWeather(nextDayForecast.Weather, nextDayForecast.Temperature);
			ThirstyEvents.OnWeatherChanged?.Invoke(dayNum + i, i, dayForecast);

			if (i == 0)		// new current day weather
				AudioSource.PlayClipAtPoint(dayForecast.Weather.MusicClip, Vector3.zero, 10f);
		}

		// last day - new random weather
		int lastDay = BusinessRecords.WeatherForecast.Count - 1;

		var randomWeather = RandomWeather;
		var lastDayForecast = BusinessRecords.WeatherForecast[lastDay];
		var randomTemp = UnityEngine.Random.Range(randomWeather.MinDayTemp, randomWeather.MaxDayTemp);

		lastDayForecast.SetWeather(randomWeather, randomTemp);
		ThirstyEvents.OnWeatherChanged?.Invoke(dayNum + lastDay, lastDay, lastDayForecast);
	}

	public WeatherSO GetWeatherType(WeatherType weatherType)
	{
		foreach (var weather in Weathers)
		{
			if (weather.WeatherType == weatherType)
				return weather;
		}

		return Weathers[0];		// default to fine
	}

	private void OnResetData()
	{
		BusinessRecords.ResetWeather(GetWeatherType(WeatherType.Fine));
	}
}
