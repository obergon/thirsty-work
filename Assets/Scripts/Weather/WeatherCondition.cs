﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// recorded in a list in BusinessRecords as 5-day forecast
[Serializable]
public class WeatherCondition
{
	public WeatherSO Weather;

	public float Temperature;

	public int StartDay;
	public int StartHour;

	public int EndDay;
	public int EndHour;

	public void SetWeather(WeatherSO weather, float temp)
	{
		Weather = weather;
		Temperature = temp;
	}

	public string WeatherDescription()
	{
		switch (Weather.WeatherType)
		{
			case WeatherController.WeatherType.Fine:
			default:
				return "Everything is Fine!";

			case WeatherController.WeatherType.Windy:
				return "That Wind is picking up!";

			case WeatherController.WeatherType.Rainy:
				return "It's Raining again...";

			case WeatherController.WeatherType.Stormy:
				return "The Storm is upon us";

			case WeatherController.WeatherType.Heatwave:
				return "What a Scorcher!";
		}
	}
}
