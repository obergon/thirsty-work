﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Person))]
public class PersonAnimator : MonoBehaviour
{
    public Animator movementAnimator;           // Samara's animations

	private const string idleKickTrigger = "IdleKick";					// not hoverboard
	private const string idleLookAroundTrigger = "IdleLookAround";
	private const string idleScratchHeadTrigger = "IdleScratchHead";
	private const string idleCheckPhoneTrigger = "IdleCheckPhone";

	private const string idleHoverBoardTrigger = "IdleHoverBoard";      // hoverboard only

	private const string walkTrigger = "Walk";                          // not hoverboard

	private float maxIdleDelay = 2f;  // to randomly offset each person's animation for freezing in different poses
	private Person person;

	private string[] idleTriggers =
	{
		idleKickTrigger,
		idleLookAroundTrigger,
		idleScratchHeadTrigger,
		idleCheckPhoneTrigger,

		walkTrigger,				// frozen!
		walkTrigger					// to increase chance of walk animation
	};
	
	private string[] hoverIdleTriggers =
	{
		idleHoverBoardTrigger,
		idleLookAroundTrigger,
		idleScratchHeadTrigger,
		idleCheckPhoneTrigger
	};

	
	private void Start()
	{
		person = GetComponent<Person>();
		StartCoroutine(RandomIdle());			// random delay to offset all people
	}

	private IEnumerator RandomIdle()
	{
		yield return new WaitForSeconds(UnityEngine.Random.Range(0f, maxIdleDelay));
		movementAnimator.SetTrigger(RandomIdleTrigger);
		yield return null;
	}

	private string RandomIdleTrigger
	{
		get
		{
			if (person.OnHoverboard)
				return hoverIdleTriggers[UnityEngine.Random.Range(0, hoverIdleTriggers.Length)];
			else
				return idleTriggers[UnityEngine.Random.Range(0, idleTriggers.Length)];
		}
	}

	private void OnEnable()
	{
		ThirstyEvents.OnDay += OnDay;
		ThirstyEvents.OnNight += OnNight;
	}

	private void OnDisable()
	{
		ThirstyEvents.OnDay -= OnDay;
		ThirstyEvents.OnNight -= OnNight;
	}

	private void OnDay()
	{
		FreezeAnimation(true);
	}

	private void OnNight()
	{
		FreezeAnimation(false);
	}

	private void FreezeAnimation(bool freeze)
	{
		movementAnimator.enabled = !freeze;
	}


	private void Walk()
	{
		if (person.OnHoverboard)
		{
			StartCoroutine(RandomIdle());		// force new random idle
			return;
		}
			
		movementAnimator.SetTrigger(walkTrigger);
	}

	private void LookAtPhone()
	{
		movementAnimator.SetTrigger(idleCheckPhoneTrigger);
	}

	private void LookAround()
	{
		movementAnimator.SetTrigger(idleLookAroundTrigger);
	}

	private void ScratchHead()
	{
		movementAnimator.SetTrigger(idleScratchHeadTrigger);
	}

	private void Kick()
	{
		if (person.OnHoverboard)
			return;
			
		movementAnimator.SetTrigger(idleKickTrigger);
	}
}