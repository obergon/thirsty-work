﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StallManager : MonoBehaviour
{
    public GameObject Stall1;
    public GameObject Server1;
    public GameObject Stall2;
	public GameObject Server2;
	public GameObject Stall3;
	public GameObject Server3;

	public AudioClip UpgradeAudio;

	public BusinessRecords BusinessRecords;

	private int currentStallLevel;


	private void OnEnable()
	{
		ThirstyEvents.OnStallUpgrade += OnStallUpgrade;

		currentStallLevel = BusinessRecords.CurrentStallLevel;
	}

	private void OnDisable()
	{
		ThirstyEvents.OnStallUpgrade += OnStallUpgrade;
	}


	private void OnStallUpgrade(int stallLevel, int coins)
	{
		if (stallLevel == currentStallLevel)
			return;

		currentStallLevel = stallLevel;

		Stall1.SetActive(stallLevel == 1);
		Server1.SetActive(stallLevel == 1);

		Stall2.SetActive(stallLevel == 2);
		Server2.SetActive(stallLevel == 2);

		Stall3.SetActive(stallLevel == 3);
		Server3.SetActive(stallLevel == 3);
	
		if (UpgradeAudio != null)
			AudioSource.PlayClipAtPoint(UpgradeAudio, Vector3.zero);
	}
}
