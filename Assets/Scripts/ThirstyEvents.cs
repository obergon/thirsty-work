﻿

public static class ThirstyEvents
{
	// Game clock

	public delegate void OnClockStartDelegate();
	public static OnClockStartDelegate OnClockStart;

	public delegate void OnClockStopDelegate();
	public static OnClockStopDelegate OnClockStop;

	public delegate void OnClockSpeedChangedDelegate(float speed);
	public static OnClockSpeedChangedDelegate OnClockSpeedChanged;

	public delegate void OnHourDelegate(int dayCount, int hourCount, int hour, int dayStartHour, int dayEndHour, float clockTickInterval);
	public static OnHourDelegate OnHour;

	public delegate void OnDayCountDelegate(int day, bool startUp);
	public static OnDayCountDelegate OnDayCount;

	public delegate void OnClockTogglePauseDelegate();
	public static OnClockTogglePauseDelegate OnClockTogglePause;

	public delegate void OnClockPausedDelegate(bool paused);
	public static OnClockPausedDelegate OnClockPaused;

	// UI

	public delegate void OnWelcomeDelegate();
	public static OnWelcomeDelegate OnWelcome;

	public delegate void OnMorningDelegate();
	public static OnMorningDelegate OnMorning;      // midnight

	public delegate void OnDayDelegate();
	public static OnDayDelegate OnDay;              // start trading

	public delegate void OnNightDelegate();
	public static OnNightDelegate OnNight;

	public delegate void OnLeaderboardDelegate();
	public static OnLeaderboardDelegate OnLeaderboard;      // TODO

	public delegate void OnInvestDelegate();
	public static OnInvestDelegate OnInvest;

	public delegate void OnFriendsDelegate();
	public static OnInvestDelegate OnFriends;

	public delegate void OnUpgradeDelegate();
	public static OnUpgradeDelegate OnUpgrade;

	public delegate void OnHideUpgradeDelegate();
	public static OnHideUpgradeDelegate OnHideUpgrade;

	public delegate void OnExpandDelegate();
	public static OnExpandDelegate OnExpand;

	public delegate void OnOrderDelegate(int totalOrderValue);
	public static OnOrderDelegate OnPlaceOrder;          // ingredients (InventoryUI)

	// Daily user settings

	public delegate void OnDrinkSelectedDelegate(DrinkInventorySO drink);
	public static OnDrinkSelectedDelegate OnDrinkSelected;

	public delegate void OnDayCupsToSellDelegate(int changeQty, int cupsToSell);
	public static OnDayCupsToSellDelegate OnDayCupsToSell;

	public delegate void OnDayAdPostersDelegate(int changeQty, int adPosters);
	public static OnDayAdPostersDelegate OnDayAdPosters;

	// Variable price per cup

	public delegate void OnPricePerCupChangedDelegate(int pricePerCup);
	public static OnPricePerCupChangedDelegate OnPricePerCupChanged;

	// Drink inventory

	public delegate void OnDrinkStockChangedDelegate(string drinkName, int changeQty, int inStock);
	public static OnDrinkStockChangedDelegate OnDrinkStockChanged;

	public delegate void OnDrinkSpoiltChangedDelegate(string drinkName, int changeQty, int spoiltQty);
	public static OnDrinkSpoiltChangedDelegate OnDrinkSpoiltChanged;

	public delegate void OnDrinkOnOrderChangedDelegate(string drinkName, int changeQty, int onOrder, int costPerCup);
	public static OnDrinkOnOrderChangedDelegate OnDrinkOnOrderChanged;

	public delegate void OnDrinkToOrderChangedDelegate(string drinkName, int changeQty, int toOrder, int costPerCup);
	public static OnDrinkToOrderChangedDelegate OnDrinkToOrderChanged;

	public delegate void OnDrinkStockIssuedDelegate(string drinkName, int changeQty, int issueQty);
	public static OnDrinkStockIssuedDelegate OnDrinkStockIssued;

	public delegate void OnDrinkUnlockedDelegate(DrinkInventorySO drink);
	public static OnDrinkUnlockedDelegate OnDrinkUnlocked;

	// Business

	public delegate void OnCoinsChangedDelegate(int coins);
	public static OnCoinsChangedDelegate OnCoinsChanged;

	public delegate void OnHourlySalesRecordDelegate(HourlyRecord record);
	public static OnHourlySalesRecordDelegate OnHourlySalesRecord;

	public delegate void OnDailySalesRecordDelegate(HourlyRecord record);
	public static OnDailySalesRecordDelegate OnDailySalesRecord;

	public delegate void OnBankruptedDelegate(int coins);
	public static OnBankruptedDelegate OnBankrupted;

	public delegate void OnSoldOutDelegate();
	public static OnSoldOutDelegate OnSoldOut;

	public delegate void OnResetDataDelegate();					// init BusinessRecords and InventorySO
	public static OnResetDataDelegate OnResetData;

	// News

	public delegate void OnRandomNewsEventDelegate(); // NewsBulletin news);
	public static OnRandomNewsEventDelegate OnRandomNewsEvent;

	// Weather

	public delegate void OnRandomWeatherEventDelegate(WeatherCondition randomWeather);
	public static OnRandomWeatherEventDelegate OnRandomWeatherEvent;

	public delegate void OnWeatherChangedDelegate(int dayNum, int forecastIndex, WeatherCondition weatherCondition);
	public static OnWeatherChangedDelegate OnWeatherChanged;

	// Upgrade

	public delegate void OnStallUpgradeDelegate(int stallLevel, int coins);
	public static OnStallUpgradeDelegate OnStallUpgrade;

	// Ads

	public delegate void OnAdWatchedDelegate();
	public static OnAdWatchedDelegate OnAdWatched;
}

