﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//#if UNITY_ADS
using UnityEngine.Advertisements;
//#endif

public class AdManager : MonoBehaviour
{
    //[SerializeField]
    private string gameId = "3543348";		// android

    //[SerializeField]
    //private bool testMode = false;

    //[SerializeField]
    private string regularAdPlacementId = "video";					// can skip

    //[SerializeField]
    private string rewardedAdPlacementId = "rewardedVideo";			// no skip


    private void Awake()
    {
        Advertisement.Initialize(gameId, false);
    }

    public void ShowRegularAd(Action<ShowResult> callback)
    {
//#if UNITY_ADS
        if (Advertisement.IsReady(regularAdPlacementId))
        {
            ShowOptions options = new ShowOptions();
            options.resultCallback = callback;
            Advertisement.Show(regularAdPlacementId, options);
        }
        else
            Debug.Log("Ad not ready - are you offline?");
//#else
//		Debug.Log("Ads not supported!");
//#endif
    }

    public void ShowRewardedAd(Action<ShowResult> callback)
    {
//#if UNITY_ADS
        if (Advertisement.IsReady(rewardedAdPlacementId))
        {
            ShowOptions options = new ShowOptions();
            options.resultCallback = callback;
            Advertisement.Show(rewardedAdPlacementId, options);
        }
        else
            Debug.Log("Ad not ready - are you offline?");
//#else
//		Debug.Log("Ads not supported!");
//#endif
    }
}